import 'exceptions.dart';

/// Thrown if the homepage response changes
///
/// If the amount of carousel changes we cannot now which carousel shows which data
class InvalidCarouselLengthException extends AniCloudException {
  /// The length that was received
  final int wrongLength;

  ///
  InvalidCarouselLengthException(this.wrongLength);

  @override
  String toString() => 'Expected 3 carousels but got $wrongLength';
}
