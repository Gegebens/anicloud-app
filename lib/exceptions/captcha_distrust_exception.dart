import 'exceptions.dart';

/// User is logged in, but captcha still appears
class CaptchaDistrustException extends AniCloudException {
  /// The cookies that need to be revalidated
  final String cookieString;

  ///
  CaptchaDistrustException(this.cookieString);
}
