import '../repositories/repositories.dart';
import 'exceptions.dart';

part 'parse_exception/base_anime.dart';
part 'parse_exception/calendar_anime_list_data.dart';
part 'parse_exception/carousel.dart';
part 'parse_exception/detailed_anime.dart';
part 'parse_exception/episode.dart';
part 'parse_exception/search_result.dart';

/// Thrown if parsing of an item from AniCloud fails
abstract class ParseException extends AniCloudException {
  final Map<String, String?> _values;

  /// Pass a key value map to find the missing value
  ParseException(this._values);

  @override
  String toString() =>
      _values.keys.map((key) => '$key => ${_values[key]}').join('\n');
}
