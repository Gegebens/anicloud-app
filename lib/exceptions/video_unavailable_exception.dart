import 'exceptions.dart';

/// Thrown if a video is unavailable from a specific host
class VideoUnavailableException extends AniCloudException {
  /// The slug of the anime
  final String slug;

  /// The episode of the anime
  final int episode;

  /// The season of the anime
  final int season;

  ///
  VideoUnavailableException(this.slug, this.episode, this.season);
}
