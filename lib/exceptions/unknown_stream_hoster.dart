import 'exceptions.dart';

/// If AniCloud adds new streamhosters, we will be informed
class UnknownStreamHosterException extends AniCloudException {
  /// The [slug] where the unknown streamhoster appeared
  final String slug;

  /// The [episode] where the unknown streamhoster appeared
  final int season;

  /// The [episode] where the unknown streamhoster appeared
  final int episode;

  ///
  UnknownStreamHosterException(this.slug, this.season, this.episode);
}
