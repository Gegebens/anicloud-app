part of '../parse_exception.dart';

/// Failed to parse [Carousel]
class CarouselParseException extends ParseException {
  CarouselParseException(Map<String, String?> values) : super(values);
}
