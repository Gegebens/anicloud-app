part of '../parse_exception.dart';

/// Failed to parse [DetailedAnime]
class DetailedAnimeParseException extends ParseException {
  ///
  DetailedAnimeParseException(Map<String, String?> values) : super(values);
}
