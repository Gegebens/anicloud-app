part of '../parse_exception.dart';

/// Failed to parse a [SearchResult]
class SearchResultParseException extends ParseException {
  ///
  SearchResultParseException(Map<String, String?> values) : super(values);
}
