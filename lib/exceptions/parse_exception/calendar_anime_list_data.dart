part of '../parse_exception.dart';

/// Failed to parse [CalendarAnimeListData]
class CalendarAnimeListDataException extends ParseException {
  ///
  CalendarAnimeListDataException(Map<String, String?> values) : super(values);
}
