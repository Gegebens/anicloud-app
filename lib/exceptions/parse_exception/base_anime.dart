part of '../parse_exception.dart';

/// Failed to parse [BaseAnime]
class BaseAnimeParseException extends ParseException {
  ///
  BaseAnimeParseException(Map<String, String?> values) : super(values);
}
