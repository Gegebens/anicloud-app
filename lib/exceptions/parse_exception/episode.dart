part of '../parse_exception.dart';

/// Failed to parse an [Episode]
class EpisodeParseException extends ParseException {
  ///
  EpisodeParseException(Map<String, String?> values) : super(values);
}
