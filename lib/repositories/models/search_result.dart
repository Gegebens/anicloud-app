import 'package:html/parser.dart' as html;

import '../../exceptions/exceptions.dart';
import '../repositories.dart';

class SearchResult {
  final String name;
  final String description;
  final String link;
  final String _cover;

  String get coverUrl => 'https://anicloud.io$_cover';

  SearchResult._(this.name, this.description, this.link, this._cover);

  ParseAble get parsed => ParseAble(slug: link, season: 1);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SearchResult &&
          runtimeType == other.runtimeType &&
          parsed.slug == other.parsed.slug;

  @override
  int get hashCode => parsed.slug.hashCode;

  static SearchResult? fromJson(Map<String, dynamic> data) {
    if (!data.containsKey('name') ||
        !data.containsKey('description') ||
        !data.containsKey('link') ||
        !data.containsKey('cover')) {
      throw SearchResultParseException(
        data.map(
          (key, value) => MapEntry(key, '$value'),
        ),
      );
    }

    final result = SearchResult._(
      html.parseFragment(data['name']).text ?? '',
      html.parseFragment(data['description']).text ?? '',
      data['link'] as String,
      data['cover'] as String,
    );

    // catch non-anime links
    try {
      result.parsed;
    } catch (_) {
      return null;
    }
    return result;
  }
}
