part of anime;

enum Language { gerDub, engSub, gerSub, unknown }

extension LanguageParser on Language {
  static Language parseFromImageFileName(String src) {
    if (src.endsWith('japanese-german.svg')) {
      return Language.gerSub;
    }
    if (src.endsWith('japanese-english.svg')) {
      return Language.engSub;
    }
    // WARNING: Don't reorder this as japanese-german ends with german.svg to
    if (src.endsWith('german.svg')) {
      return Language.gerDub;
    }
    return Language.unknown;
  }
}
