part of anime;

class BaseAnime {
  final String _imageUrl;
  final String title;
  final String url;

  ParseAble get parsed => ParseAble.fromString(url);

  String get slug => parsed.slug;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BaseAnime &&
          runtimeType == other.runtimeType &&
          title == other.title;

  @override
  int get hashCode => title.hashCode;

  String get image => 'https://anicloud.io$_imageUrl';

  const BaseAnime(
    this._imageUrl, {
    required this.title,
    required this.url,
  });

  factory BaseAnime.parse(Element element) {
    final title = element.querySelector('h3')?.text.trim();
    final imgElementAttrs = element.querySelector('img')?.attributes;
    final imageUrl = imgElementAttrs?['data-src'] ?? imgElementAttrs?['src'];
    final url = element.attributes['href'];
    if (title != null && imageUrl != null && url != null) {
      return BaseAnime(
        imageUrl,
        title: title,
        url: url,
      );
    }
    throw BaseAnimeParseException({
      'imageUrl': imageUrl,
      'title': title,
      'url': url,
    });
  }

  CalendarAnime toCalendarAnime(
    int season,
    int episode,
    String time,
    Language language,
  ) {
    return CalendarAnime(
      _imageUrl,
      title: title,
      url: url,
      season: season,
      releasedEpisode: episode,
      time: time,
      releasedLanguage: language,
    );
  }
}
