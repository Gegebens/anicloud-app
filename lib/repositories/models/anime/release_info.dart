part of anime;

abstract class ReleaseInfo {
  final int releasedEpisode;
  final Language releasedLanguage;

  ReleaseInfo(this.releasedEpisode, this.releasedLanguage);
}
