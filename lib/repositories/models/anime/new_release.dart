part of anime;

class NewRelease extends DetailedAnime implements ReleaseInfo {
  @override
  final int releasedEpisode;
  @override
  final Language releasedLanguage;

  NewRelease(
    String imageUrl, {
    required String title,
    required String url,
    required String description,
    required Iterable<int> seasons,
    required Iterable<Episode> episodes,
    required Iterable<String> genres,
    required String? trailerUrl,
    required int startYear,
    required int endYear,
    required int seriesId,
    required this.releasedEpisode,
    required this.releasedLanguage,
  }) : super(
          imageUrl,
          title: title,
          url: url,
          description: description,
          seasons: seasons,
          episodes: episodes,
          genres: genres,
          trailerUrl: trailerUrl,
          startYear: startYear,
          endYear: endYear,
          seriesId: seriesId,
        );
}
