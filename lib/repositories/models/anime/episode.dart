part of anime;

class Episode {
  final String germanTitle;
  final String englishTitle;
  final Iterable<Language> languages;
  final int episode;
  final int episodeId;
  final bool watched;

  Episode(
    this.germanTitle,
    this.englishTitle,
    this.languages,
    this.episode,
    this.episodeId,
    // ignore: avoid_positional_boolean_parameters
    this.watched,
  );

  factory Episode.parse(Element element) {
    final episode =
        int.tryParse(element.attributes['data-episode-season-id'] ?? '-');
    final episodeId =
        int.tryParse(element.attributes['data-episode-id'] ?? '-');
    final germanTitle =
        element.querySelector('.seasonEpisodeTitle > a > strong')?.text ?? '';
    final englishTitle =
        element.querySelector('.seasonEpisodeTitle > a > span')?.text ?? '';
    final languages = element.querySelectorAll('.editFunctions img').map((e) {
      final src = e.attributes['src'] ?? '';
      return LanguageParser.parseFromImageFileName(src);
    });
    final hasSeenEpisode = element.classes.contains('seen');
    if (episode != null && episodeId != null) {
      return Episode(
        germanTitle,
        englishTitle,
        languages,
        episode,
        episodeId,
        hasSeenEpisode,
      );
    }
    throw EpisodeParseException({
      'germanTitle': germanTitle,
      'englishTitle': englishTitle,
      'languages': languages.map((e) => e.toString()).join(', '),
      'episode': '$episode',
      'episodeId': '$episodeId',
    });
  }
}
