part of anime;

class ReleaseUpgradeAble extends UpgradeAble implements ReleaseInfo {
  @override
  final int releasedEpisode;
  @override
  final Language releasedLanguage;

  ReleaseUpgradeAble(
      ParseAble toFetch, this.releasedEpisode, this.releasedLanguage)
      : super(toFetch);
}
