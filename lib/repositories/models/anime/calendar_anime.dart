part of anime;

class CalendarAnime extends BaseAnime implements ReleaseInfo {
  @override
  final int releasedEpisode;
  @override
  final Language releasedLanguage;
  final int season;
  final String time;

  CalendarAnime(
    String imageUrl, {
    required String title,
    required String url,
    required this.season,
    required this.releasedEpisode,
    required this.time,
    required this.releasedLanguage,
  }) : super(
          imageUrl,
          title: title,
          url: url,
        );

  factory CalendarAnime.parse(Element element) {
    final base = BaseAnime.parse(element);

    final smallItems = element.querySelectorAll('small');
    final rawSeasonEpisodeComposite = smallItems.at(0)?.text ?? '-';
    final matchedSeason =
        RegExp(r'S(\d*)E(\d*)').firstMatch(rawSeasonEpisodeComposite);
    final season = int.tryParse(matchedSeason?.group(1) ?? '-') ?? -1;
    final episode = int.tryParse(matchedSeason?.group(2) ?? '-') ?? -1;
    final rawLang =
        smallItems.at(0)?.querySelector('img')?.attributes['data-src'];
    final lang = LanguageParser.parseFromImageFileName(rawLang ?? '-');
    final rawTimeString = smallItems.last.text;
    final time =
        RegExp(r'~\s*(\d\d:\d\d)\s*Uhr').firstMatch(rawTimeString)?.group(1) ??
            '--:--';

    return base.toCalendarAnime(season, episode, time, lang);
  }
}
