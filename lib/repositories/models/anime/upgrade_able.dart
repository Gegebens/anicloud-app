part of anime;

class UpgradeAble {
  final ParseAble toFetch;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UpgradeAble &&
          runtimeType == other.runtimeType &&
          toFetch == other.toFetch;

  @override
  int get hashCode => toFetch.hashCode;

  UpgradeAble(this.toFetch);
}
