part of anime;

class ParseAble {
  final String slug;
  final int season;
  final int? episode;

  bool get isMovie => season == -1;

  String get toShare => 'https://anicloud.io/anime/stream'
      '/$slug/'
      '${isMovie ? 'filme' : 'staffel-$season'}/'
      '${episode != null ? isMovie ? 'film-$episode' : 'episode-$episode' : ''}';

  const ParseAble({
    required this.slug,
    required this.season,
    this.episode,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ParseAble &&
          runtimeType == other.runtimeType &&
          slug == other.slug;

  @override
  int get hashCode => slug.hashCode;

  factory ParseAble.fromString(String data) {
    // /anime/stream/{slug}/staffel-{staffel}/episode-{episode}
    // /anime/stream/{slug}/filme/film-{episode}
    final regex = RegExp(
        r'\/anime\/stream\/([a-z0-9-_]*)(\/(staffel-(\d{1,})|filme)(\/(episode|film)-(\d{1,}))?)?');
    final match = regex.firstMatch(data);
    if (match == null) {
      throw FormatException(data);
    }
    final slug = match.group(1);

    final isMovie = match.group(3) == 'filme';

    final season = isMovie ? -1 : int.tryParse(match.group(4) ?? '1') ?? 1;
    final episode = int.tryParse(match.group(7) ?? '-');
    if (slug != null) {
      return ParseAble(slug: slug, season: season, episode: episode);
    }
    throw FormatException(data);
  }

  @override
  String toString() => 'slug: $slug\nseason: $season\nepisode: $episode';
}
