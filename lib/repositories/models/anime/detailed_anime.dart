part of anime;

class DetailedAnime extends BaseAnime {
  final String description;
  final Iterable<int> seasons;
  final Iterable<Episode> episodes;
  final Iterable<String> genres;
  final int startYear;
  final int endYear;
  final int seriesId;
  final String? trailerUrl;

  // Detailed Page Selectors
  static const _detailedTitle = '.series-title > h1 > span';
  static const _detailedImage = '.seriesCoverBox > img';
  static const _detailedDescription = 'p.seri_des';
  static const _detailedGenresTag = '.genres > ul > li > a';
  static const _detailedSeasonsElement = '#stream > ul';
  static const _detailedSeasons = 'li > a';
  static const _detailedEpisodes = '.seasonEpisodesList > tbody > tr';
  static const _detailedStartDate = 'span[itemprop=startDate] > a';
  static const _detailedEndDate = 'span[itemprop=endDate] > a';
  static const _detailedTrailerButton = '.trailerButton';

  DetailedAnime(
    String imageUrl, {
    required String title,
    required String url,
    required this.description,
    required this.seasons,
    required this.episodes,
    required this.genres,
    required this.trailerUrl,
    required this.startYear,
    required this.endYear,
    required this.seriesId,
  }) : super(imageUrl, title: title, url: url);

  factory DetailedAnime.parse(Document document, Uri url) {
    final title = document.querySelector(_detailedTitle)?.text;
    final imageUrl =
        document.querySelector(_detailedImage)?.attributes['data-src'];
    final description = html
        .parseFragment(document
                .querySelector(_detailedDescription)
                ?.attributes['data-full-description'] ??
            '')
        .text;
    final genres =
        document.querySelectorAll(_detailedGenresTag).map((e) => e.text);
    final seasons = document
        .querySelectorAll(_detailedSeasonsElement)
        .at(0)
        ?.querySelectorAll(_detailedSeasons)
        .map((e) => int.tryParse(e.text) ?? -1);
    final episodes = document
        .querySelectorAll(_detailedEpisodes)
        .map((e) => Episode.parse(e));
    final startYear =
        int.tryParse(document.querySelector(_detailedStartDate)?.text ?? '-') ??
            DateTime.now().year;
    final endYear =
        int.tryParse(document.querySelector(_detailedEndDate)?.text ?? '-') ??
            DateTime.now().year;
    final trailerUrl =
        document.querySelector(_detailedTrailerButton)?.attributes['href'];
    final seriesId = int.parse(
      document.querySelector('.add-series')?.attributes['data-series-id'] ??
          '-',
    );
    if (imageUrl != null &&
        title != null &&
        description != null &&
        seasons != null) {
      final anime = DetailedAnime(
        imageUrl,
        title: title,
        url: url.toString(),
        description: description,
        seasons: seasons,
        episodes: episodes,
        genres: genres,
        trailerUrl: trailerUrl,
        startYear: startYear,
        endYear: endYear,
        seriesId: seriesId,
      );
      return anime;
    } else {
      throw DetailedAnimeParseException({
        'imageUrl': imageUrl,
        'title': title,
        'url': url.toString(),
        'description': description,
        'seasons': seasons?.map((e) => e.toString()).join(', '),
        'genres': genres.join(', '),
        'trailerUrl': trailerUrl,
        'startYear': '$startYear',
        'endYear': '$endYear',
        'seriesId': '$seriesId',
      });
    }
  }

  NewRelease toNewRelease(int episode, Language language) {
    return NewRelease(
      _imageUrl,
      title: title,
      url: url,
      description: description,
      seasons: seasons,
      episodes: episodes,
      genres: genres,
      trailerUrl: trailerUrl,
      startYear: startYear,
      endYear: endYear,
      seriesId: seriesId,
      releasedEpisode: episode,
      releasedLanguage: language,
    );
  }
}
