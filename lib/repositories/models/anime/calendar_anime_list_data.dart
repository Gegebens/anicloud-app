part of anime;

class CalendarAnimeListData {
  final String day;
  final DateTime date;
  final Iterable<CalendarAnime> animes;

  CalendarAnimeListData(this.day, this.date, this.animes);

  factory CalendarAnimeListData.parse(Element element) {
    final rawDateString = element.querySelector('h3')?.text;
    final dateStringParsed = RegExp(
            r'(Montag|Dienstag|Mittwoch|Donnerstag|Freitag|Samstag|Sonntag),\s(\d\d).(\d\d).(\d{4})')
        .firstMatch(rawDateString ?? '');

    final dayString = dateStringParsed?.group(1);
    final day = int.tryParse(dateStringParsed?.group(2) ?? '-');
    final month = int.tryParse(dateStringParsed?.group(3) ?? '-');
    final year = int.tryParse(dateStringParsed?.group(4) ?? '-');

    final animes = element
        .querySelectorAll('a')
        .map((e) => CalendarAnime.parse(e))
        .notNull();
    if (dayString != null && year != null && month != null && day != null) {
      return CalendarAnimeListData(
        dayString,
        DateTime(year, month, day),
        animes,
      );
    }
    throw CalendarAnimeListDataException({
      'dayString': dayString,
      'year': '$year',
      'month': '$month',
      'day': '$day'
    });
  }
}
