import 'dart:io';

import 'package:http/http.dart' as http;

import '../../util/util.dart';
import '../repositories.dart';

class HostData {
  final String redirectPath;
  final Language language;
  final Host host;

  String get redirectLink => 'https://anicloud.io$redirectPath';

  HostData(this.redirectPath, this.language, this.host);
}

/// Contains the requested data from [HostResultInfo]
class StreamResultInfo<E> {
  /// Streaming host
  final Host host;

  /// Data-Source
  final ResponsePage<E> source;

  ///
  StreamResultInfo({required this.host, required this.source});
}

class HostResultInfo {
  final Host host;
  final String resolved;

  HostResultInfo(this.resolved, this.host);
}

/// A cache entry for video stream urls
class StreamCacheEntry {
  /// The temporary video file info
  final StreamResultInfo info;

  /// The stream url for the video
  final HostResultInfo originalData;

  ///
  StreamCacheEntry(this.info, this.originalData);

  /// Checks if the cache is still valid
  Future<bool> get isValid async =>
      (await http.head(Uri.parse(info.source.source!))).statusCode ==
      HttpStatus.ok;
}
