library anime;

import 'package:html/dom.dart';
import 'package:html/parser.dart' as html;

import '../../exceptions/exceptions.dart';
import '../../extensions/extensions.dart';

part 'anime/base_anime.dart';
part 'anime/calendar_anime.dart';
part 'anime/calendar_anime_list_data.dart';
part 'anime/detailed_anime.dart';
part 'anime/episode.dart';
part 'anime/language.dart';
part 'anime/new_release.dart';
part 'anime/parse_able.dart';
part 'anime/release_info.dart';
part 'anime/release_upgrade_able.dart';
part 'anime/upgrade_able.dart';
