import 'package:html/dom.dart';

import '../../exceptions/exceptions.dart';
import 'anime.dart';

class Carousel {
  final String name;
  final Iterable<BaseAnime> animes;

  Carousel(this.name, this.animes);

  static const _coverListItem = '.coverListItem > a';

  factory Carousel.parse(Element element) {
    final animes = element
        .querySelectorAll(_coverListItem)
        .map(
          (e) => BaseAnime.parse(e),
        )
        .toSet();
    final name = element.querySelector('h2')?.text;
    if (name != null) {
      return Carousel(name, animes);
    }
    throw CarouselParseException({'name': name});
  }
}
