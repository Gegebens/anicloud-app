import 'package:html/dom.dart';

import 'carousel.dart';

class HomeResponse {
  final Iterable<Carousel> carousels;

  HomeResponse(this.carousels);

  static const _carousels = '.carousel:not(.animeNews)';

  factory HomeResponse.parse(Document document) {
    final carousels =
        document.querySelectorAll(_carousels).map((e) => Carousel.parse(e));
    return HomeResponse(carousels);
  }
}
