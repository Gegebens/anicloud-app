import 'dart:async';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' as html;
import 'package:loggy/loggy.dart';
import 'package:path_provider/path_provider.dart';

import '../exceptions/exceptions.dart';
import '../extensions/extensions.dart';
import '../providers/detailed_cache_provider.dart';
import '../providers/providers.dart';
import '../util/util.dart';
import 'models/models.dart';

final aniCloudRepositoryProvider =
    Provider((ref) => AniCloudRepository(ref.read));

class AniCloudRepository with UiLoggy {
  static const _baseUrl = 'https://anicloud.io';
  final Dio _client = Dio();
  final Reader _reader;
  late final CookieManager _manager;

  Future<void> init() async {
    final documentDir = await getApplicationDocumentsDirectory();

    _manager =
        CookieManager(PersistCookieJar(storage: FileStorage(documentDir.path)));
    _client.interceptors.add(_manager);
    loggy.info('CookieJar added to client');
  }

  AniCloudRepository(this._reader);

  Uri get _home => Uri.parse(_baseUrl);

  Uri get _newEpisodes => Uri.parse('$_baseUrl/neue-episoden');

  Uri get _calendar => Uri.parse('$_baseUrl/animekalender');

  Uri get _setWatchList => Uri.parse('$_baseUrl/ajax/setWatchList');

  Uri get _getWatchList => Uri.parse('$_baseUrl/account/watchlist');

  Uri get _all => Uri.parse('$_baseUrl/animes-alphabet');

  Uri get _login => Uri.parse('$_baseUrl/login');

  Uri get _markSeen => Uri.parse('$_baseUrl/ajax/lastseen');

  Uri _search(String keyword) => Uri.parse(
      '$_baseUrl/ajax/seriesSearch?keyword=${Uri.encodeComponent(keyword)}');

  // Uri get _search => Uri.parse('$_baseUrl/ajax/search');

  Uri _detailed(String slug, int season) {
    final seasonAttached = season == -1 ? 'filme' : 'staffel-$season';
    final url = '$_baseUrl/anime/stream/$slug/$seasonAttached';
    return Uri.parse(url);
  }

  Uri _episode(String slug, int season, int episode) {
    return Uri.parse(
        '${_detailed(slug, season).toString()}/${season == -1 ? 'film' : 'episode'}-$episode');
  }

  // New Episode Page
  static const _newEpisodesSelector =
      '.newEpisodeList > .rows > .col-md-12 > .row > .col-md-12';

  // Detailed Page
  static const _episodeHostData = '.hosterSiteVideo > .row > li';

  // Calendar Page
  static const _calendarSection = '#seriesContainer > .calendarList';

  void _refreshAccountState(Document element) {
    final userName = element.querySelector('.name')?.text;
    final imageUrl = element.querySelector('.avatar img')?.attributes['src'];

    final newAccountState = () {
      if (userName != null && imageUrl != null) {
        return LoggedInAccountState(userName, imageUrl);
      } else {
        return LoggedOutAccountState();
      }
    }();
    final currentState = _reader(accountProvider).state;
    if (newAccountState != currentState) {
      loggy.info(
          'Account State changed from ${currentState.readable} to ${newAccountState.readable}');
      _reader(accountProvider).state = newAccountState;
    }
  }

  bool get isLoggedIn => _reader(accountProvider).state is LoggedInAccountState;

  Future<Iterable<UpgradeAble>> getAllAnimes() async {
    loggy.debug('Getting a-z page');
    final response = await _client.getUri(_all);
    final parsed = html.parse(response.data);
    _refreshAccountState(parsed);
    return parsed.querySelectorAll('#seriesContainer ul a').map((e) {
      final link = e.attributes['href'];
      if (link != null) {
        return UpgradeAble(ParseAble.fromString(link));
      }
      return null;
    }).notNull();
  }

  Future<Iterable<SearchResult>> search(String query) async {
    final response = await _client.getUri<List>(_search(query));

    final decoded = response.data ?? [];
    return decoded
        .map((e) => SearchResult.fromJson(e as Map<String, dynamic>))
        .notNull()
        .toSet();
  }

  Future<Iterable<CalendarAnimeListData>> getCalendar() async {
    loggy.debug('Getting calendar page');
    final response = await _client.getUri(_calendar);
    final parsed = html.parse(response.data);
    _refreshAccountState(parsed);
    final sections = parsed
        .querySelectorAll(_calendarSection)
        .map((e) => CalendarAnimeListData.parse(e));
    return sections;
  }

  Future<String?> resolveRedirectLink(HostData hostData) async {
    loggy.debug('Trying to resolve redirect link via account');
    try {
      final response = await _client.get(
        hostData.redirectLink,
        options: Options(
          followRedirects: false,
          validateStatus: (s) => s == HttpStatus.movedPermanently,
        ),
      );
      if (!(response.isRedirect ?? true)) {
        return null;
      }
      return response.headers.value(HttpHeaders.locationHeader);
    } on DioError catch (e) {
      loggy.warning('''Captcha doesn't trust us anymore''');
      if (e.response?.statusCode != HttpStatus.movedPermanently) {
        final cookieString = (await _manager.cookieJar.loadForRequest(_home))
            .map((e) => '${e.name}=${e.value}')
            .join(';');
        throw CaptchaDistrustException(cookieString);
      }
      rethrow;
    }
  }

  Future<Iterable<HostData>> getHostData({
    required String slug,
    required int season,
    required int episode,
  }) async {
    loggy.debug(
        'Getting HostData\nslug: $slug\nseason: $season\nepisode: $episode');
    final response = await _client.getUri(_episode(slug, season, episode));
    final parsed = html.parse(response.data);
    return parsed.querySelectorAll(_episodeHostData).map((e) {
      final host = HostExt.parse(
          e.querySelector('.generateInlinePlayer > a > h4')?.text ?? '-');
      final language = Language.values
          .elementAt(int.parse(e.attributes['data-lang-key'] ?? '-') - 1);
      final redirectLink = e.attributes['data-link-target'] ?? '';
      return HostData(redirectLink, language, host);
    });
  }

  Future<Iterable<ReleaseUpgradeAble>> getLatestReleases() async {
    loggy.debug('Getting latest releases');
    final response = await _client.getUri(_newEpisodes);
    final parsed = html.parse(response.data);
    _refreshAccountState(parsed);
    return parsed.querySelectorAll(_newEpisodesSelector).map((e) {
      final parsed = ParseAble.fromString(
        e.querySelector('a')?.attributes['href'] ?? '',
      );
      final langSrc = LanguageParser.parseFromImageFileName(
        e.querySelector('img')?.attributes['data-src'] ?? '-',
      );

      return ReleaseUpgradeAble(parsed, parsed.episode!, langSrc);
    });
  }

  Future<DetailedAnime> getDetailedAnime({
    required String slug,
    int season = 1,
  }) async {
    final url = _detailed(slug, season);

    final cache = DetailedCacheProvider().get(url);
    if (cache != null) {
      return cache;
    }
    loggy.debug('Fetching DetailedAnime\nslug: $slug\nseason: $season');

    final response = await _client.getUri(url);
    final parsed = html.parse(response.data);
    _refreshAccountState(parsed);
    final anime = DetailedAnime.parse(parsed, url);
    DetailedCacheProvider().set(url, anime);
    return anime;
  }

  Future<HomeResponse> getHomeResponse() async {
    loggy.debug('Getting home response');
    final response = await _client.getUri(_home);
    final parsed = html.parse(response.data);
    _refreshAccountState(parsed);
    return HomeResponse.parse(parsed);
  }

  Future<void> logout() {
    loggy.info('Clearing cookie jar to logout user');
    return _manager.cookieJar.deleteAll();
  }

  Future<bool> login({required String email, required String password}) async {
    loggy.debug('Logging in user with $email');
    final response = await _client.postUri<List<int>>(
      _login,
      data: {
        'email': email,
        'password': password,
      },
      options: Options(
        contentType: 'application/x-www-form-urlencoded',
        responseType: ResponseType.bytes,
      ),
    );
    // If response is empty it succeeded
    return (response.data?.length ?? 1) == 0;
  }

  Future<bool> _changeWatchListStatus(int seriesId) async {
    final response = await _client.postUri<Map<String, dynamic>>(
      _setWatchList,
      data: {'series': '$seriesId'},
      options: Options(
        contentType: 'application/x-www-form-urlencoded',
      ),
    );
    return (response.data?['status'] as bool?) ?? false;
  }

  Future<bool> changeWatchListStatus(ParseAble parseAble) async {
    loggy.debug('Changing watchlist status for\n$parseAble');
    final detailed = await getDetailedAnime(slug: parseAble.slug);
    return _changeWatchListStatus(detailed.seriesId);
  }

  Future<Iterable<BaseAnime>> getWatchList() async {
    final response = await _client.getUri(_getWatchList);
    final parsed = html.parse(response.data);
    final seriesList = parsed.querySelector('.seriesListContainer');
    loggy.debug('Retrieving watchlist');
    if (seriesList == null) {
      return const Iterable.empty();
    }
    return seriesList.querySelectorAll('a').map((e) => BaseAnime.parse(e));
  }

  Future<void> markEpisodeAsSeen(int episodeId) async {
    loggy.debug('Marking episode as watched\nepisodeId: $episodeId');
    await _client.postUri(
      _markSeen,
      data: {'episode': '$episodeId'},
      options: Options(
        contentType: 'application/x-www-form-urlencoded',
      ),
    );
  }
}
