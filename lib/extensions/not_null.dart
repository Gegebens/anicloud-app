/// Null helpers
extension NotNull<E> on Iterable<E?> {
  /// Returns all element in this list that are not null
  Iterable<E> notNull() sync* {
    for (final item in this) {
      if (item != null) {
        yield item;
      }
    }
  }
}

/// Out of range helpers
extension SafeAccess<E> on Iterable<E> {
  /// Try to access the element at [index], if it is out of range, return null
  E? at(int index) {
    if (length <= index) {
      return null;
    }
    return elementAt(index);
  }
}
