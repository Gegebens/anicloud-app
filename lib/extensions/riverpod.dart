import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../widgets/widgets.dart';

/// Riverpod helpers
extension DefaultAsyncValue<E> on AsyncValue<E> {
  /// Returns [data] when data arrives
  ///
  /// In case of loading a [LoadingMessageWidget] is displayed
  /// In case of error an [ErrorMessageWidget] is displayed
  Widget resolve({required Widget Function(E) data}) {
    return when(
      data: data,
      loading: () => const LoadingMessageWidget(),
      error: (e, s) => ErrorMessageWidget(
        error: e,
        stackTrace: s,
      ),
    );
  }
}
