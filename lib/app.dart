import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:loggy/loggy.dart';

import 'pages/all_animes.dart';
import 'pages/pages.dart';
import 'repositories/repositories.dart';
import 'routes.dart';
import 'util/util.dart';
import 'widgets/widgets.dart';

final _initProvider = FutureProvider<void>((ref) async {
  await Future.wait([
    Hive.initFlutter()
        .then((value) => Future.wait(kBoxes.map((e) => e.open()))),
    ref.read(aniCloudRepositoryProvider).init(),
  ]);
});

/// Root widget
class App extends ConsumerWidget with UiLoggy {
  ///
  const App({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(_initProvider).when(
      data: (_) {
        return MaterialApp(
          title: 'AniCloud',
          navigatorKey: kNavigatorKey,
          onGenerateRoute: (settings) {
            Route result(Widget Function(BuildContext) builder) {
              return Platform.isAndroid
                  ? MaterialPageRoute(builder: builder)
                  : CupertinoPageRoute(builder: builder);
            }

            return result((context) {
              if (settings.name?.contains('anime/stream') ?? false) {
                try {
                  final info = ParseAble.fromString(settings.name ?? '');
                  final args = settings.arguments as DetailedArguments?;
                  return Detailed(
                    parseAble: info,
                    url: settings.name ?? '',
                    fortune: args?.fortune ?? -1,
                    imageUrl: args?.imageUrl,
                  );
                } catch (e, s) {
                  loggy.error('Failed to parse anicloud url', e, s);
                }
              }
              switch (settings.name) {
                case VideoPlayerPage.routeName:
                  return const VideoPlayerPage();
                case NetworkShare.routeName:
                  return const NetworkShare();
                case AllAnimes.routeName:
                  return const AllAnimes();
                case LoginPage.routeName:
                  return LoginPage();
                case AccountPage.routeName:
                  return const AccountPage();
                case Logs.routeName:
                  return const Logs();
                default:
                  return const Home();
              }
            });
          },
          theme: kThemeData,
        );
      },
      loading: () {
        return MaterialApp(
          home: Scaffold(
            body: LoadingMessageWidget(
              height: null,
              child: Image.asset('assets/images/splash.png', width: 280),
            ),
          ),
          theme: kThemeData,
        );
      },
      error: (e, s) {
        return MaterialApp(
          home: Scaffold(
            body: ErrorMessageWidget(
              error: e,
              stackTrace: s,
            ),
          ),
          theme: kThemeData,
        );
      },
    );
  }
}
