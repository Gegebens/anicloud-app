import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';

class PlayPauseButton extends ConsumerStatefulWidget {
  final double iconSize;

  const PlayPauseButton({
    Key? key,
    this.iconSize = 70,
  }) : super(key: key);

  @override
  ConsumerState createState() => _PlayPauseButtonState();
}

class _PlayPauseButtonState extends ConsumerState<PlayPauseButton>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _playPause;

  @override
  void initState() {
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
    _playPause = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  //ignore: avoid_positional_boolean_parameters
  void handleChange(bool playing) {
    if (_controller.status == AnimationStatus.completed && playing) {
      _controller.reverse();
    } else if (_controller.status == AnimationStatus.dismissed && !playing) {
      _controller.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final shouldUseNetworkShare =
        ref.watch(networkShareProvider.select((value) => value.isReady));
    ref.listen<bool>(
      shouldUseNetworkShare
          ? networkShareProvider
              .select((value) => value.remoteVideoState.playing)
          : videoPlayerControllerProvider
              .select((value) => value.value.isPlaying),
      handleChange,
    );
    if (shouldUseNetworkShare &&
        !ref.read(networkShareProvider).remoteVideoState.playing) {
      _controller.forward(from: 0);
    }
    return AnimatedBuilder(
      builder: (context, snapshot) {
        return IconButton(
          iconSize: widget.iconSize,
          icon: AnimatedIcon(
            icon: AnimatedIcons.pause_play,
            progress: _playPause,
          ),
          onPressed: () {
            if (shouldUseNetworkShare) {
              final controller = ref.read(networkShareProvider);
              controller.playPause();
            } else {
              final controller = ref.read(videoPlayerControllerProvider);
              if (controller.value.isPlaying) {
                controller.pause();
              } else {
                controller.play();
              }
            }
          },
        );
      },
      animation: _controller,
    );
  }
}
