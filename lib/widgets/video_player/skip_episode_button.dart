import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';

/// Skip button for [VideoPlayerPage], skips to the next episode with [action]
class SkipEpisodeButton extends ConsumerWidget {
  /// Which direction to skip
  final SkipAction action;

  final double iconSize;

  ///
  const SkipEpisodeButton({Key? key, required this.action, this.iconSize = 50})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final canSkip = ref.watch(canSkipProvider(action));
    return IconButton(
      iconSize: iconSize,
      icon: Icon(
        action == SkipAction.backward
            ? Icons.skip_previous_rounded
            : Icons.skip_next_rounded,
      ),
      onPressed: canSkip
          ? () async {
              ref
                  .read(
                      episodeChooserProvider(ref.read(currentParseAbles).state))
                  .state += action == SkipAction.forward ? 1 : -1;
              ref.read(streamProvider).replacePlayer();
            }
          : null,
    );
  }
}
