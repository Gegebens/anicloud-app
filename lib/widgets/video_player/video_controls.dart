import 'package:flutter/material.dart';

import '../../providers/providers.dart';
import '../widgets.dart';

class VideoControls extends StatelessWidget {
  const VideoControls({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: const <Widget>[
        SkipEpisodeButton(action: SkipAction.backward),
        SkipButton(action: SkipAction.backward),
        PlayPauseButton(),
        SkipButton(action: SkipAction.forward),
        SkipEpisodeButton(action: SkipAction.forward)
      ],
    );
  }
}
