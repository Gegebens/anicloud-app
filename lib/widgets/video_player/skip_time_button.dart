import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../pages/pages.dart';
import '../../providers/providers.dart';

/// Skip button for [VideoPlayerPage], skips 10 seconds into [action]
class SkipButton extends ConsumerStatefulWidget {
  /// Which direction to skip
  final SkipAction action;

  ///
  const SkipButton({Key? key, required this.action}) : super(key: key);

  @override
  _SkipButtonState createState() => _SkipButtonState();
}

class _SkipButtonState extends ConsumerState<SkipButton>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _animation;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    _animation = CurvedAnimation(
        parent: _controller, curve: const Cubic(0.42, 0.0, 1.0, 0.0));
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return Transform.rotate(
          angle: widget.action == SkipAction.backward
              ? -(_animation.value * math.pi)
              : (_animation.value * math.pi),
          child: child,
        );
      },
      child: IconButton(
        iconSize: 50,
        icon: Icon(widget.action == SkipAction.forward
            ? Icons.forward_10_rounded
            : Icons.replay_10_rounded),
        onPressed: () async {
          final networkController = ref.read(networkShareProvider);
          if (networkController.isReady) {
            networkController
                .seekValue(widget.action == SkipAction.forward ? 10 : -10);
          } else {
            final videoController = ref.read(videoPlayerControllerProvider);
            var skip = videoController.value.position;
            if (widget.action == SkipAction.forward) {
              skip += const Duration(seconds: 10);
            } else {
              skip -= const Duration(seconds: 10);
            }
            videoController.seekTo(skip);
          }
          await _controller.forward(from: 0);
          await _controller.reverse(from: 1);
        },
      ),
    );
  }
}
