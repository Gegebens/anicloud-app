import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';
import '../../util/util.dart';

class VideoPlayerAppBar extends ConsumerWidget {
  const VideoPlayerAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final parseAble = ref.read(currentParseAbles).state;
    final episode = ref.watch(episodeChooserProvider(parseAble)).state;
    final season = ref.watch(seasonChooserProvider(parseAble)).state;
    final title = ref.read(detailedPageProvider)!.data!.value.title;
    final hostData = ref.read(videoSourceProvider).state!.host;

    return Align(
      alignment: Alignment.topLeft,
      child: Row(
        children: <Widget>[
          IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              '$title - S${season}E$episode via ${hostData.readable}',
              style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
