import 'package:flutter/material.dart';
import 'package:screen_brightness/screen_brightness.dart';
import 'package:wakelock/wakelock.dart';

import '../../pages/pages.dart';

/// Creates a slider in the [VideoPlayerPage] to control the brightness
///
/// Wake lock will be enabled on creation. The start brightness will
/// be restored and Wake lock will get disabled on dispose.
class BrightnessSlider extends StatefulWidget {
  const BrightnessSlider({Key? key}) : super(key: key);

  @override
  _BrightnessSliderState createState() => _BrightnessSliderState();
}

class _BrightnessSliderState extends State<BrightnessSlider> {
  double _brightness = 0;

  @override
  void initState() {
    Wakelock.enable();
    ScreenBrightness.current.then((value) {
      if (mounted) {
        _brightness = value;
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    Wakelock.disable();
    ScreenBrightness.resetScreenBrightness();

    super.dispose();
  }

  double get brightness => _brightness;

  set brightness(double brightness) {
    setState(() {
      _brightness = brightness;
    });
    ScreenBrightness.setScreenBrightness(brightness);
  }

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                RotatedBox(
                  quarterTurns: -1,
                  child: SliderTheme(
                    data: const SliderThemeData(
                      thumbColor: Colors.white,
                      disabledThumbColor: Colors.transparent,
                      overlappingShapeStrokeColor: Colors.transparent,
                      thumbShape: RoundSliderThumbShape(
                        enabledThumbRadius: 4.5,
                        elevation: 0,
                        pressedElevation: 0,
                      ),
                      activeTrackColor: Colors.white,
                      inactiveTrackColor: Colors.grey,
                      overlayColor: Colors.transparent,
                      trackHeight: 7,
                    ),
                    child: Slider(
                      value: brightness,
                      onChanged: (value) => brightness = value,
                    ),
                  ),
                ),
              ],
            ),
            const Padding(
              padding: EdgeInsets.only(left: 13),
              child: Icon(
                Icons.brightness_high_rounded,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
