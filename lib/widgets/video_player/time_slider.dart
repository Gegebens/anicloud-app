import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';

String _formatSeconds(int millis) {
  final hours = millis / 3600000;
  final minutes = (hours - hours.truncateToDouble()) * 60;
  final seconds = (minutes - minutes.truncateToDouble()) * 60;

  return '${hours >= 1 ? '${hours.truncate()}:'.padLeft(3, '0') : ''}'
      '${'${minutes.truncate().toString().padLeft(2, '0')}'
          ':${seconds.truncate().toString().padLeft(2, '0')}'}';
}

class TimeSlider extends ConsumerWidget {
  const TimeSlider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final shouldUseNetwork =
        ref.watch(networkShareProvider.select((value) => value.isReady));
    late int position;
    late int duration;
    var buffered = 0;
    if (shouldUseNetwork) {
      final currentValue = ref.watch(
          networkShareProvider.select((value) => value.remoteVideoState));
      final overrideValue = ref.watch(networkShareProvider
          .select((value) => value.overridePosition?.floor()));
      position = (overrideValue ?? currentValue.currentTime.floor()) * 1000;
      duration = currentValue.duration.floor() * 1000;
    } else {
      final currentValue = ref.watch(videoPlayerControllerProvider).value;
      position = currentValue.position.inMilliseconds;
      duration = currentValue.duration.inMilliseconds;
      if (currentValue.buffered.isNotEmpty) {
        buffered = currentValue.buffered.first.end.inMilliseconds;
      }
    }
    final String posSeconds = _formatSeconds(position);
    final String endSeconds = _formatSeconds(duration - position);

    final child = Row(
      children: <Widget>[
        Text(posSeconds),
        Expanded(
          child: Stack(
            children: [
              Positioned.fill(
                child: Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: LinearProgressIndicator(
                      color: Theme.of(context)
                          .colorScheme
                          .primary
                          .withOpacity(0.6),
                      value: buffered == 0 ? 0 : buffered / duration,
                    ),
                  ),
                ),
              ),
              SliderTheme(
                data: const SliderThemeData(
                    inactiveTrackColor: Colors.transparent),
                child: Slider(
                  value: position / 1000 > 0 ? (position / 1000) : 0,
                  onChanged: (position) {
                    if (shouldUseNetwork) {
                      ref.read(networkShareProvider).overridePosition =
                          position;
                    } else {
                      ref
                          .read(videoPlayerControllerProvider)
                          .seekTo(Duration(seconds: position.floor()));
                    }
                  },
                  onChangeEnd: (position) {
                    if (shouldUseNetwork) {
                      ref.read(networkShareProvider).seek(position);
                      ref.read(networkShareProvider).overridePosition = null;
                    }
                  },
                  max: duration / 1000 > 0 ? duration / 1000 : 1,
                  divisions: (duration ~/ 1000) > 0 ? (duration ~/ 1000) : 1,
                  label: posSeconds,
                ),
              ),
            ],
          ),
        ),
        Text('-$endSeconds'),
      ],
    );

    return shouldUseNetwork
        ? child
        : Positioned(
            bottom: 2,
            left: 20,
            child: SizedBox(
              width: MediaQuery.of(context).size.width - 40,
              child: child,
            ),
          );
  }
}
