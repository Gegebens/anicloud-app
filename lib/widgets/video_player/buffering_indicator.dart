import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';

class BufferingIndicator extends ConsumerWidget {
  const BufferingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loading = ref.watch(videoPlayerControllerProvider
        .select((value) => value.value.isBuffering));
    return loading
        ? const Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: 20, right: 20),
              child: SizedBox(
                width: 25,
                height: 25,
                child: CircularProgressIndicator(
                  strokeWidth: 5,
                ),
              ),
            ),
          )
        : const SizedBox();
  }
}
