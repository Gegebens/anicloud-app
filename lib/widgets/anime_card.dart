import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/network_share_provider.dart';
import '../providers/providers.dart';
import '../repositories/repositories.dart';
import '../util/util.dart';
import 'widgets.dart';

class AnimeCard extends ConsumerWidget {
  final BaseAnime anime;
  final ValueChanged<String>? callback;
  final double? width;
  final double fakeImageHeight;

  const AnimeCard({
    Key? key,
    required this.anime,
    this.callback,
    this.width = 150,
    this.fakeImageHeight = 225,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final fortune = Random().nextInt(99999);
    return Card(
      child: Stack(
        children: [
          Column(
            children: [
              Stack(
                fit: StackFit.passthrough,
                children: [
                  SizedBox(
                    height: fakeImageHeight,
                    child: Hero(
                      tag: '${anime.url}$fortune',
                      child: CachedNetworkImage(
                        fit: BoxFit.fitHeight,
                        imageUrl: anime.image,
                        errorWidget: (context, _, __) => Container(),
                        placeholder: (context, _) => Container(
                          height: fakeImageHeight,
                          width: width,
                          color: const Color(0xFF343434),
                        ),
                      ),
                    ),
                  ),
                  Positioned.fill(
                    child: AnimeCardProgressIndicator(parseAble: anime.parsed),
                  ),
                  if (anime is ReleaseInfo) ...[
                    Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: Colors.purple,
                            borderRadius: anime is! CalendarAnime
                                ? const BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                  )
                                : null,
                          ),
                          child: Text(
                              'EP ${(anime as ReleaseInfo).releasedEpisode}'),
                        ),
                        if (anime is CalendarAnime)
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: const BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10),
                              ),
                            ),
                            child: Text('~${(anime as CalendarAnime).time}'),
                          ),
                      ],
                    ),
                    Positioned.fill(
                      child: Align(
                        // hover over the bottom right
                        alignment: const Alignment(0.95, 0.95),
                        child: Flag(
                          language: (anime as ReleaseInfo).releasedLanguage,
                        ),
                      ),
                    )
                  ]
                ],
              ),
              const Spacer(),
              SizedBox(
                width: width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: AutoSizeText(
                    anime.title,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                  ),
                ),
              ),
              const Spacer(),
            ],
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: callback != null
                    ? () => callback!(anime.url)
                    : () {
                        if (ref.read(networkShareProvider).canStream &&
                            ref.read(detailedPageProvider)?.data?.value.slug !=
                                anime.slug) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              content: Text(
                                  'Du streamst im Moment schon einen Anime,\n'
                                  'beende das Streamen, um einen neuen Anime zu öffnen'),
                            ),
                          );
                        } else {
                          Navigator.of(context).pushNamed(
                            anime.url,
                            arguments: DetailedArguments(fortune, anime.image),
                          );
                        }
                      },
                child: Container(
                  alignment: Alignment.topRight,
                  decoration: const BoxDecoration(
                    gradient: RadialGradient(
                      radius: 0.4,
                      center: Alignment.topRight,
                      colors: [
                        Color(0x55343434),
                        Colors.transparent,
                      ],
                    ),
                  ),
                  child: FavoriteButton(parseAble: anime.parsed),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
