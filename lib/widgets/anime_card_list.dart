import 'package:flutter/material.dart';

import '../repositories/repositories.dart';
import 'widgets.dart';

class AnimeCardList<E> extends StatelessWidget {
  final Iterable<E> animes;
  final String title;

  const AnimeCardList({
    Key? key,
    required this.animes,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.headline5,
          ),
          if (animes.isNotEmpty)
            SizedBox(
              height: 300,
              child: ListView.builder(
                itemCount: animes.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  final item = animes.elementAt(index);
                  if (item is UpgradeAble) {
                    return LazyAnimeCard(value: item);
                  }
                  if (item is BaseAnime) {
                    return AnimeCard(anime: item);
                  }
                  throw UnimplementedError();
                },
              ),
            )
          else
            SizedBox(
              height: 300,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Center(),
                  Text(
                    'Pretty Empty in here',
                    style: Theme.of(context).textTheme.headline4,
                    textAlign: TextAlign.center,
                  ),
                  const Icon(Icons.sentiment_dissatisfied, size: 60)
                ],
              ),
            )
        ],
      ),
    );
  }
}
