import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../extensions/extensions.dart';
import '../../providers/providers.dart';
import '../../repositories/repositories.dart';

class EpisodeChooser extends ConsumerWidget {
  const EpisodeChooser({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final episodes = ref.watch(episodesProvider);
    return episodes.resolve(
      data: (data) {
        if (data == null || data.isEmpty) {
          return const SizedBox();
        }
        final parseAble = ref.read(currentParseAbles).state;
        final chosenEpisode =
            ref.watch(episodeChooserProvider(parseAble)).state;
        final totalEpisodes = data.length;
        return Column(
          children: [
            Consumer(
              builder: (BuildContext context, WidgetRef ref, Widget? child) {
                final currentEpisode = chosenEpisode - 1;
                final selectedLanguage =
                    ref.watch(currentLanguageProvider).state;

                final mapping = <Language, Text>{};
                final languages = data.at(currentEpisode)?.languages ?? [];
                for (final lang in languages) {
                  mapping.putIfAbsent(
                      lang, () => Text(lang.toString().split('.').last));
                }
                final isAvailable = languages.contains(selectedLanguage);
                if (!isAvailable) {
                  SchedulerBinding.instance?.addPostFrameCallback((_) {
                    ref.read(currentLanguageProvider).state = languages.first;
                  });
                }

                return mapping.length == 1
                    ? Text(languages.first.toString().split('.').last)
                    : CupertinoSlidingSegmentedControl<Language>(
                        children: mapping,
                        onValueChanged: (value) {
                          if (value != null) {
                            ref.read(currentLanguageProvider).state = value;
                          }
                        },
                        groupValue:
                            isAvailable ? selectedLanguage : languages.first,
                      );
              },
            ),
            Row(
              children: <Widget>[
                IconButton(
                  icon: const Icon(Icons.remove),
                  onPressed: chosenEpisode > 1
                      ? () => ref
                          .read(episodeChooserProvider(parseAble))
                          .state = chosenEpisode - 1
                      : null,
                ),
                Visibility(
                  visible: totalEpisodes != 1,
                  child: Expanded(
                    child: Slider(
                      activeColor: Colors.greenAccent,
                      value: chosenEpisode.toDouble(),
                      onChanged: (value) => ref
                          .read(episodeChooserProvider(parseAble))
                          .state = value.toInt(),
                      min: 1,
                      max: totalEpisodes.toDouble(),
                      divisions: totalEpisodes <= 1 ? 1 : totalEpisodes - 1,
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: chosenEpisode < totalEpisodes
                      ? () => ref
                          .read(episodeChooserProvider(parseAble))
                          .state = chosenEpisode + 1
                      : null,
                ),
              ],
            ),
            AutoSizeText(
              '${data.elementAt(chosenEpisode - 1).germanTitle} - ${data.elementAt(chosenEpisode - 1).englishTitle}',
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
            OutlinedButton(
              onPressed: () async {
                ref.read(streamProvider).openPlayer();
              },
              child: Text('Episode $chosenEpisode abspielen'),
            )
          ],
        );
      },
    );
  }
}
