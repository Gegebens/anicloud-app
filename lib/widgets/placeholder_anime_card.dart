import 'package:flutter/material.dart';

class PlaceholderAnimeCard extends StatelessWidget {
  const PlaceholderAnimeCard({
    Key? key,
    required this.fakeImageHeight,
    required this.width,
  }) : super(key: key);

  final double fakeImageHeight;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          Container(
            height: fakeImageHeight,
            width: width,
            color: const Color(0xFF343434),
          ),
          const SizedBox(height: 25),
          Container(
            margin: const EdgeInsets.all(8.0),
            height: 20,
            width: 100,
            color: const Color(0xFF343434),
          ),
        ],
      ),
    );
  }
}
