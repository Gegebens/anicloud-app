import 'package:flutter/material.dart';
import 'package:loggy/loggy.dart';

class ErrorMessageWidget extends StatelessWidget with UiLoggy {
  final dynamic error;
  final StackTrace? stackTrace;

  const ErrorMessageWidget({
    Key? key,
    this.error,
    this.stackTrace,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    loggy.error('Error caught during build', error, stackTrace);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Center(),
        Text('Ein Fehler ist aufgetreten'),
        Icon(Icons.error, size: 40),
      ],
    );
  }
}
