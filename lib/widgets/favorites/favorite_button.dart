import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../providers/providers.dart';
import '../../repositories/repositories.dart';

class FavoriteButton extends ConsumerWidget {
  final ParseAble parseAble;

  const FavoriteButton({Key? key, required this.parseAble}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ValueListenableBuilder<Box<String>>(
      valueListenable:
          Hive.box<String>('favorites').listenable(keys: [parseAble.slug]),
      builder: (context, box, _) {
        final isFavorite = box.containsKey(parseAble.slug);
        return IconButton(
          icon: Icon(
            isFavorite ? Icons.favorite : Icons.favorite_border,
            color: isFavorite ? Colors.red : null,
          ),
          onPressed: () {
            _updateAniCloud(ref, isFavorite, context);
            _updateLocalStorage(isFavorite, box, context);
          },
        );
      },
    );
  }

  Future<void> _updateAniCloud(
      WidgetRef ref, bool isFavorite, BuildContext context) async {
    final isLoggedIn = ref.read(accountProvider).state is LoggedInAccountState;
    if (isLoggedIn) {
      await ref.watch(watchListProvider).when(
            data: (watchList) =>
                _updateRemoteWatchList(watchList!, isFavorite, ref),
            loading: () => Future.value(),
            error: (e, s) {
              _showUnavailableMessage(context);
              return Future.value();
            },
          );
    }
  }

  Future<void> _updateRemoteWatchList(
      Iterable<BaseAnime> watchList, bool isFavorite, WidgetRef ref) async {
    final isInWatchList =
        watchList.any((element) => element.parsed == parseAble);
    if (isInWatchList && isFavorite || !isInWatchList && !isFavorite) {
      await ref
          .read(aniCloudRepositoryProvider)
          .changeWatchListStatus(parseAble);
      ref.read(watchListRefreshProvider).refresh();
    }
  }

  void _showUnavailableMessage(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content:
            Text('WatchList ist nicht verfügbar, versuch es später nochmal'),
      ),
    );
  }

  void _updateLocalStorage(
      bool isFavorite, Box<String> box, BuildContext context) {
    if (!isFavorite) {
      box.put(parseAble.slug, parseAble.slug);
    } else {
      box.delete(parseAble.slug);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: const Text('Element von den Favoriten entfernt'),
          action: SnackBarAction(
            label: 'RÜCKGÄNGIG',
            onPressed: () {
              box.put(parseAble.slug, parseAble.slug);
            },
          ),
        ),
      );
    }
  }
}
