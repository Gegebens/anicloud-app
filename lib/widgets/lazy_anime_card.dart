import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/providers.dart';
import '../repositories/repositories.dart';
import 'anime_card.dart';
import 'widgets.dart';

class LazyAnimeCard extends ConsumerWidget {
  final UpgradeAble value;
  final ValueChanged<String>? callback;
  final double? width;
  final double fakeImageHeight;

  const LazyAnimeCard({
    Key? key,
    required this.value,
    this.callback,
    this.width = 150,
    this.fakeImageHeight = 225,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref
        .watch(detailedGetter(value.toFetch))
        .whenData(
          (resolved) => value is ReleaseUpgradeAble
              ? resolved.toNewRelease(
                  (value as ReleaseUpgradeAble).releasedEpisode,
                  (value as ReleaseUpgradeAble).releasedLanguage,
                )
              : resolved,
        )
        .when(
      data: (data) {
        return AnimeCard(
          anime: data,
          width: width,
          fakeImageHeight: fakeImageHeight,
        );
      },
      loading: () {
        return PlaceholderAnimeCard(
          fakeImageHeight: fakeImageHeight,
          width: width,
        );
      },
      error: (e, s) {
        return const SizedBox();
      },
    );
  }
}
