import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'loading_widget.dart';

class CaptchaDialog extends StatefulWidget {
  final String redirectLink;
  final String? cookies;

  const CaptchaDialog({
    Key? key,
    required this.redirectLink,
    required this.cookies,
  }) : super(key: key);

  @override
  _CaptchaDialogState createState() => _CaptchaDialogState();
}

class _CaptchaDialogState extends State<CaptchaDialog> {
  int _showIndex = 0;

  @override
  void initState() {
    Timer(const Duration(seconds: 10), () {
      if (mounted) {
        setState(() {
          _showIndex = 1;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Captcha...'),
      content: SizedBox(
        height: 200,
        width: 200,
        child: IndexedStack(
          index: _showIndex,
          children: [
            const LoadingMessageWidget(height: 200),
            WebView(
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (c) {
                c.loadUrl(
                  widget.redirectLink,
                  headers: {
                    HttpHeaders.cookieHeader: widget.cookies ?? '',
                  },
                );
              },
              navigationDelegate: (dec) async {
                if (!dec.url.contains('anicloud.io') &&
                    !dec.url.contains('google.com/recaptcha') &&
                    !dec.url.contains('about:blank')) {
                  Navigator.of(context).pop(dec.url);
                }
                return NavigationDecision.navigate;
              },
            ),
          ],
        ),
      ),
    );
  }
}
