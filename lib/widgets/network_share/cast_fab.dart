import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../pages/pages.dart';
import '../../providers/providers.dart';

enum _ConnectionState { connected, ready, streaming, offline }

class CastFloatingActionButton extends ConsumerWidget {
  const CastFloatingActionButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ctrl = ref.watch(networkShareProvider.select(
      (value) => value.canStream
          ? _ConnectionState.streaming
          : value.isConnected
              ? _ConnectionState.connected
              : value.isReady
                  ? _ConnectionState.ready
                  : _ConnectionState.offline,
    ));
    return Padding(
      padding:
          EdgeInsets.only(bottom: ctrl == _ConnectionState.streaming ? 64 : 0),
      child: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed(NetworkShare.routeName);
        },
        child: Icon(
          ctrl == _ConnectionState.connected ||
                  ctrl == _ConnectionState.streaming
              ? Icons.connected_tv
              : ctrl == _ConnectionState.ready
                  ? Icons.cast_connected
                  : Icons.cast,
        ),
      ),
    );
  }
}
