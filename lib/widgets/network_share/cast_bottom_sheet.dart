import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../pages/pages.dart';
import '../../providers/providers.dart';
import '../widgets.dart';

class CastBottomSheet extends ConsumerWidget {
  const CastBottomSheet({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final canStream =
        ref.watch(networkShareProvider.select((value) => value.canStream));
    if (canStream) {
      final detailedAnime = ref.read(detailedPageProvider)!.data!.value;
      return SizedBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              onTap: () =>
                  Navigator.of(context).pushNamed(NetworkShare.routeName),
              leading: const PlayPauseButton(
                iconSize: 40,
              ),
              title: Text(detailedAnime.title),
              subtitle: Consumer(
                builder: (context, ref, child) {
                  final parseAble = ref.read(currentParseAbles).state;
                  final episode =
                      ref.watch(episodeChooserProvider(parseAble)).state;
                  final season =
                      ref.watch(seasonChooserProvider(parseAble)).state;
                  return Text('S${season}E$episode');
                },
              ),
              trailing: const SkipEpisodeButton(
                action: SkipAction.forward,
                iconSize: 45,
              ),
            ),
            Consumer(
              builder: (context, ref, child) {
                final remoteState = ref.watch(networkShareProvider
                    .select((value) => value.remoteVideoState));
                return LinearProgressIndicator(
                  value: remoteState.currentTime / remoteState.duration,
                );
              },
            )
          ],
        ),
      );
    }
    return const SizedBox();
  }
}
