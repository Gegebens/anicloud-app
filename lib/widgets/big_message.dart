import 'package:flutter/material.dart';

class BigMessage extends StatelessWidget {
  final IconData icon;
  final String message;

  const BigMessage({Key? key, required this.icon, required this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Center(),
        Icon(
          icon,
          size: 70,
          color: Colors.grey.withOpacity(0.3),
        ),
        Text(
          message,
          style: TextStyle(
            fontSize: 25,
            color: Colors.grey.withOpacity(0.3),
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
