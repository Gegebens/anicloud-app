import 'package:flutter/material.dart';

import '../repositories/repositories.dart';

class Flag extends StatelessWidget {
  final Language language;

  const Flag({Key? key, required this.language}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (language) {
      case Language.gerDub:
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 10,
              width: 50,
              decoration: const BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(4),
                ),
              ),
            ),
            Container(color: Colors.red, height: 10, width: 50),
            Container(
              height: 10,
              width: 50,
              decoration: const BoxDecoration(
                color: Colors.yellow,
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(4),
                ),
              ),
            ),
          ],
        );
      case Language.engSub:
      case Language.gerSub:
        return Container(
          height: 30,
          width: 50,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: Colors.white,
          ),
          padding: const EdgeInsets.all(6),
          child: const CircleAvatar(
            backgroundColor: Colors.red,
          ),
        );
      case Language.unknown:
        throw UnimplementedError();
    }
  }
}
