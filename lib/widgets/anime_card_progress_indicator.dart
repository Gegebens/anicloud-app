import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/providers.dart';
import '../repositories/repositories.dart';

class AnimeCardProgressIndicator extends ConsumerWidget {
  final ParseAble parseAble;

  const AnimeCardProgressIndicator({Key? key, required this.parseAble})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final data = ref.watch(progressProvider(parseAble)).state;
    final episode = ref.read(episodeChooserProvider(parseAble)).state;
    final season = ref.read(seasonChooserProvider(parseAble)).state;
    return data != null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.black, Colors.transparent],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
                child: Text(
                  'S$season E$episode',
                  textAlign: TextAlign.center,
                ),
              ),
              LinearProgressIndicator(value: data)
            ],
          )
        : const SizedBox();
  }
}
