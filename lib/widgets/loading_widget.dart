import 'package:flutter/material.dart';

class LoadingMessageWidget extends StatelessWidget {
  final double? height;
  final Widget? child;

  const LoadingMessageWidget({
    Key? key,
    this.height = 290,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Center(),
          if (child != null) child!,
          const Text('Loading...'),
          const CircularProgressIndicator(),
        ],
      ),
    );
  }
}
