import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class SettingsEntry extends StatelessWidget {
  final String settingKey;
  final String title;
  final String? subtitle;
  final bool defaultValue;

  const SettingsEntry({
    Key? key,
    this.defaultValue = false,
    required this.title,
    required this.settingKey,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<bool>>(
      valueListenable:
          Hive.box<bool>('settings').listenable(keys: [settingKey]),
      builder: (context, box, _) {
        final value =
            box.get(settingKey, defaultValue: defaultValue) ?? defaultValue;
        return ListTile(
          title: Text(title),
          subtitle: subtitle != null ? Text(subtitle!) : null,
          trailing: Switch.adaptive(
            value: value,
            onChanged: (newValue) {
              box.put(settingKey, newValue);
            },
          ),
        );
      },
    );
  }
}
