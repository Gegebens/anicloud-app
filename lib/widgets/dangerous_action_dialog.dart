import 'package:flutter/material.dart';

import 'widgets.dart';

class DangerousActionDialog extends StatefulWidget {
  final String title;
  final String description;
  final String cancelText;
  final String continueText;
  final Future Function() action;

  const DangerousActionDialog({
    Key? key,
    required this.title,
    required this.description,
    required this.cancelText,
    required this.continueText,
    required this.action,
  }) : super(key: key);

  @override
  State<DangerousActionDialog> createState() => _DangerousActionDialogState();
}

class _DangerousActionDialogState extends State<DangerousActionDialog> {
  bool _isRunning = false;

  @override
  Widget build(BuildContext context) {
    if (_isRunning) {
      return FutureBuilder(
        future: widget.action(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return AlertDialog(
              title: const Text('Fehler'),
              content: const Text('Aktion fehlgeschlagen'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Ok'),
                ),
              ],
            );
          }

          if (snapshot.connectionState == ConnectionState.done) {
            return AlertDialog(
              title: const Text('Fertig'),
              content: const Text('Aktion abgeschlossen'),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Ok'),
                ),
              ],
            );
          }
          return const AlertDialog(
            title: Text('Laden...'),
            content: LoadingMessageWidget(),
          );
        },
      );
    }
    return AlertDialog(
      title: Text(widget.title),
      content: Text(widget.description),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(widget.cancelText),
        ),
        ElevatedButton(
          onPressed: () => setState(() => _isRunning = true),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.red),
            foregroundColor: MaterialStateProperty.all(Colors.white),
          ),
          child: Text(widget.continueText),
        )
      ],
    );
  }
}
