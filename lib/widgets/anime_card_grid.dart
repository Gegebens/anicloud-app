import 'package:flutter/material.dart';

import '../repositories/repositories.dart';
import 'widgets.dart';

/// Grid for [CustomSearchDelegate] and [FavoritePage] results
class AnimeCardGrid<E> extends StatelessWidget {
  /// The [Anime]s to display into the grid
  final Iterable<E> animes;

  final ValueChanged<String>? callback;

  ///
  const AnimeCardGrid({
    Key? key,
    required this.animes,
    this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return GridView.builder(
        itemCount: animes.length,
        itemBuilder: (context, index) {
          final item = animes.elementAt(index);
          if (item is UpgradeAble) {
            return LazyAnimeCard(
                value: item, width: null, fakeImageHeight: 300);
          }
          if (item is BaseAnime) {
            return AnimeCard(anime: item, fakeImageHeight: 300);
          }
          throw UnimplementedError();
        },
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: constraints.maxWidth > 600
              ? constraints.maxWidth / 4
              : constraints.maxWidth / 2,
          childAspectRatio: 8.3 / 16,
        ),
      );
    });
  }
}
