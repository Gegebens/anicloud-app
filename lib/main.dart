import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_loggy/flutter_loggy.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loggy/loggy.dart';

import 'app.dart';

void main() {
  Loggy.initLoggy(
    logPrinter: StreamPrinter(const PrettyDeveloperPrinter()),
  );
  runZonedGuarded(
    () {
      FlutterError.onError = (details) {
        if (!details.silent && kReleaseMode) {
          Loggy(' Error ')
              .error('Error occured', details.exception, details.stack);
        }
        FlutterError.dumpErrorToConsole(details);
      };
      runApp(const ProviderScope(child: App()));
    },
    (e, s) {
      Loggy(' Error ').error('Error occured', e, s);
    },
  );
}
