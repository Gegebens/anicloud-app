import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:network_info_plus/network_info_plus.dart';

import '../providers/providers.dart';
import '../widgets/widgets.dart';

class NetworkShare extends ConsumerWidget {
  static const routeName = '/network/share';

  const NetworkShare({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ctrl = ref.watch(networkShareProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Network Share'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Center(),
          if (ctrl.isConnected)
            Column(
              children: [
                const Center(),
                const Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 40,
                ),
                Text(
                  'Mit Browser verbunden',
                  style: Theme.of(context).textTheme.headline6,
                ),
                if (ctrl.canStream) ...[
                  const VideoControls(),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.85,
                    child: const TimeSlider(),
                  )
                ]
              ],
            )
          else if (ctrl.isReady)
            Column(
              children: [
                const Center(),
                const Icon(
                  Icons.tv,
                  color: Colors.orange,
                  size: 40,
                ),
                Text(
                  'Warte auf Verbindung…',
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(height: 50),
                FutureBuilder<String?>(
                  future: NetworkInfo().getWifiIP(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      final url = 'http://${snapshot.data}:8080';
                      return Column(
                        children: [
                          InkWell(
                            child: Text(
                              url,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  ?.copyWith(color: Colors.blueAccent),
                              textAlign: TextAlign.center,
                            ),
                            onTap: () {
                              Clipboard.setData(ClipboardData(text: url));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                content: Text('Copied to clipboard'),
                                behavior: SnackBarBehavior.floating,
                              ));
                            },
                          ),
                          Text(
                            '(Click to copy)',
                            style: Theme.of(context).textTheme.caption,
                          )
                        ],
                      );
                    }
                    if (snapshot.hasError ||
                        snapshot.connectionState == ConnectionState.done) {
                      return const Text(
                        'Error getting IP, are you connected to a network',
                      );
                    }
                    return const Text('Loading…');
                  },
                )
              ],
            )
          else
            Image.asset('assets/images/splash.png', height: 105),
          const SizedBox(height: 50),
          OutlinedButton.icon(
            onPressed: () {
              ctrl.isReady ? ctrl.cleanup() : ctrl.startCast();
            },
            label: Text(ctrl.isReady ? 'Stop' : 'Start'),
            icon: Icon(
              ctrl.isReady ? Icons.close : Icons.cast,
            ),
          )
        ],
      ),
    );
  }
}
