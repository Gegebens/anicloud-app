import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../extensions/extensions.dart';
import '../providers/providers.dart';
import '../widgets/widgets.dart';

/// An a-z list with all animes
class AllAnimes extends ConsumerWidget {
  ///
  static const routeName = '/all';

  ///
  const AllAnimes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final response = ref.watch(allAnimesProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Alle Animes'),
      ),
      body: response.resolve(
        data: (data) => AnimeCardGrid(animes: data),
      ),
    );
  }
}
