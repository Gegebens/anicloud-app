import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:video_player/video_player.dart';

import '../../providers/providers.dart';
import '../../widgets/widgets.dart';

/// The video player
class VideoPlayerPage extends ConsumerStatefulWidget {
  ///
  static const routeName = '/video';

  ///
  const VideoPlayerPage({
    Key? key,
  }) : super(key: key);

  @override
  _VideoPlayerPageState createState() => _VideoPlayerPageState();
}

class _VideoPlayerPageState extends ConsumerState<VideoPlayerPage> {
  Timer? _closer;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitUp,
    ]);
    SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.manual,
      overlays: SystemUiOverlay.values,
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: SafeArea(
        child: GestureDetector(
          onTapUp: (_) {
            final hiddenController = ref.read(hideControlsProvider);
            if (hiddenController.state) {
              hiddenController.state = false;
              _closer = Timer(const Duration(seconds: 6), () {
                _closer = null;
                hiddenController.state = true;
              });
            } else {
              _closer?.cancel();
              hiddenController.state = true;
            }
          },
          child: Stack(
            children: [
              Consumer(
                builder: (BuildContext context, WidgetRef ref, Widget? child) {
                  final controller = ref.watch(videoPlayerControllerProvider);
                  if (controller.value.isInitialized) {
                    return Center(
                      child: AspectRatio(
                        aspectRatio: controller.value.aspectRatio,
                        child: VideoPlayer(controller),
                      ),
                    );
                  } else {
                    return child!;
                  }
                },
                child: const LoadingMessageWidget(),
              ),
              const BufferingIndicator(),
              Consumer(
                builder: (BuildContext context, WidgetRef ref, Widget? child) {
                  final hide = ref.watch(hideControlsProvider).state;
                  return IgnorePointer(
                    ignoring: hide,
                    child: AnimatedOpacity(
                      opacity: hide ? 0 : 1,
                      duration: const Duration(milliseconds: 300),
                      child: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          buildShadowOverlay(),
                          // const NetworkShareButton(),
                          const BrightnessSlider(),
                          const VideoPlayerAppBar(),
                          const TimeSlider(),
                          buildControls(),
                        ],
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Align buildControls() {
    return const Align(
      child: VideoControls(),
    );
  }

  Material buildShadowOverlay() {
    return const Material(
      color: Colors.black38,
    );
  }
}
