import 'package:flutter/material.dart';
import 'package:flutter_loggy/flutter_loggy.dart';

class Logs extends StatelessWidget {
  static const routeName = '/logs';

  const Logs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const LoggyStreamScreen();
  }
}
