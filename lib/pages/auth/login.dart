import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/providers.dart';
import '../../repositories/repositories.dart';
import '../../widgets/widgets.dart';
import '../pages.dart';

/// Login users
class LoginPage extends ConsumerWidget {
  ///
  static const routeName = '/login';
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  ///
  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/splash.png', height: 80),
            const SizedBox(height: 50),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _emailController,
                decoration: const InputDecoration(
                  hintText: 'jens.spahn@gmail.com',
                  labelText: 'E-Mail Addresse',
                  prefixIcon: Icon(Icons.email),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _passwordController,
                decoration: const InputDecoration(
                  labelText: 'Passwort',
                  prefixIcon: Icon(Icons.lock),
                ),
                obscureText: true,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return FutureBuilder<bool>(
                      future: ref.read(aniCloudRepositoryProvider).login(
                            email: _emailController.text,
                            password: _passwordController.text,
                          ),
                      builder: (context, snapshot) {
                        return AlertDialog(
                          title: Text(snapshot.hasError
                              ? 'Error Loggin in'
                              : snapshot.hasData
                                  ? snapshot.data!
                                      ? 'Logged in'
                                      : 'Login failed'
                                  : 'Login...'),
                          content: snapshot.hasError
                              ? const Text('Login Failed, try again later...')
                              : snapshot.hasData
                                  ? snapshot.data!
                                      ? Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: const [
                                            Text('Login succeeded!'),
                                            Icon(
                                              Icons.check_circle,
                                              color: Colors.green,
                                              size: 50,
                                            ),
                                          ],
                                        )
                                      : const Text(
                                          'Login failed, try other creds...',
                                        )
                                  : const LoadingMessageWidget(),
                          actions: [
                            if (snapshot.hasData)
                              TextButton(
                                onPressed: () {
                                  ref.read(refreshProvider).refresh();
                                  Navigator.of(context).pop();
                                  if (snapshot.data!) {
                                    Navigator.of(context).pushReplacementNamed(
                                      AccountPage.routeName,
                                    );
                                  }
                                },
                                child: const Text('OK'),
                              )
                          ],
                        );
                      },
                    );
                  },
                );
              },
              child: const Text('Login'),
            )
          ],
        ),
      ),
    );
  }
}
