import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';

import '../../providers/providers.dart';
import '../../repositories/repositories.dart';
import '../../widgets/widgets.dart';

/// After returning to the homescreen, what did happen?
enum RefreshType {
  /// Nothing (null) is returned
  none,

  /// The user was logged out, the [TabController] needs to be updated
  logout,
}

/// Displays the Username and the Profile Picture
class AccountPage extends ConsumerWidget {
  ///
  static const routeName = '/account';

  ///
  const AccountPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final acc = ref.watch(accountProvider).state;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Account'),
      ),
      body: acc is LoggedInAccountState
          ? Column(
              children: [
                const Center(),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.orangeAccent,
                    child: acc.imageUrl.endsWith('svg')
                        ? Text(
                            acc.userName.substring(0, 2),
                            style: Theme.of(context).textTheme.headline5,
                          )
                        : Image.network(acc.imageUrl),
                  ),
                ),
                Text(
                  acc.userName,
                  style: Theme.of(context).textTheme.headline4,
                ),
                const Spacer(),
                Text(
                  'Watchlisten Syncen',
                  style: Theme.of(context).textTheme.headline5,
                ),
                ListTile(
                  leading: const Icon(Icons.file_upload),
                  title: const Text('"Put To Cloud"'),
                  subtitle: const Text('Ersetzt alle Cloud Einträge '
                      'mit deiner lokalen Favoriten Liste'),
                  onTap: () {
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) => DangerousActionDialog(
                        title: 'Warnung',
                        description:
                            'Diese Aktion kann nicht rückgängig gemacht werden, und löscht alle Einträge bei AniCloud, die nicht lokal gespeichert sind!',
                        cancelText: 'Zurück',
                        continueText: 'Fortfahren',
                        action: () async {
                          final aniCloudRepo =
                              ref.read(aniCloudRepositoryProvider);
                          final remoteSnapshot =
                              (await aniCloudRepo.getWatchList())
                                  .map((e) => e.parsed);
                          final slugs = Hive.box<String>('favorites')
                              .values
                              .map((e) => ParseAble(slug: e, season: 1));

                          final toRemove = remoteSnapshot
                              .where((element) => !slugs.contains(element));
                          final toAdd = slugs.where(
                              (element) => !remoteSnapshot.contains(element));

                          await Future.wait([...toRemove, ...toAdd]
                              .map(aniCloudRepo.changeWatchListStatus));
                          ref.read(watchListRefreshProvider).refresh();
                        },
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.compare_arrows),
                  title: const Text('"Sync Both"'),
                  subtitle: const Text(
                      'Lädt alle Cloud Einträge in die lokale Favoriten Liste'
                      ' und fügt alle lokalen Einträge in die Cloud'),
                  onTap: () {
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) => DangerousActionDialog(
                        title: 'Warnung',
                        description:
                            'Diese Aktion kann nicht rückgängig gemacht werden, und kann je nach größe der WatchList länger dauern!',
                        cancelText: 'Zurück',
                        continueText: 'Fortfahren',
                        action: () async {
                          final aniCloudRepo =
                              ref.read(aniCloudRepositoryProvider);
                          final remoteSnapshot =
                              (await aniCloudRepo.getWatchList())
                                  .map((e) => e.parsed);
                          final slugs = Hive.box<String>('favorites')
                              .values
                              .map((e) => ParseAble(slug: e, season: 1));

                          final toAddLocal = remoteSnapshot
                              .where((element) => !slugs.contains(element));
                          final toAddRemote = slugs.where(
                              (element) => !remoteSnapshot.contains(element));

                          await Future.wait(
                            [
                              ...toAddRemote
                                  .map(aniCloudRepo.changeWatchListStatus),
                              ...toAddLocal.map(
                                (localAdd) => Hive.box<String>('favorites').put(
                                  localAdd.slug,
                                  localAdd.slug,
                                ),
                              )
                            ],
                          );
                          ref.read(watchListRefreshProvider).refresh();
                        },
                      ),
                    );
                  },
                ),
                ListTile(
                  leading: const Icon(Icons.file_download),
                  title: const Text('"Put to local"'),
                  subtitle: const Text(
                      'Ersetzt alle lokalen Einträge mit den Cloud Einträgen'),
                  onTap: () {
                    showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) => DangerousActionDialog(
                        title: 'Warnung',
                        description:
                            'Diese Aktion kann nicht rückgängig gemacht werden, und löscht alle lokalen Einträge, die nicht bei AniCloud gespeichert sind!',
                        cancelText: 'Zurück',
                        continueText: 'Fortfahren',
                        action: () async {
                          final aniCloudRepo =
                              ref.read(aniCloudRepositoryProvider);
                          final remoteSnapshot =
                              (await aniCloudRepo.getWatchList())
                                  .map((e) => e.parsed);
                          final slugs = Hive.box<String>('favorites')
                              .values
                              .map((e) => ParseAble(slug: e, season: 1));

                          final toDelete = slugs.where(
                              (element) => !remoteSnapshot.contains(element));
                          final toAdd = remoteSnapshot
                              .where((element) => !slugs.contains(element));

                          Hive.box<String>('favorites')
                              .deleteAll(toDelete.map((e) => e.slug));
                          for (final localAdd in toAdd) {
                            Hive.box<String>('favorites')
                                .put(localAdd.slug, localAdd.slug);
                          }
                          ref.read(watchListRefreshProvider).refresh();
                        },
                      ),
                    );
                  },
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton.icon(
                    onPressed: () async {
                      Navigator.of(context).pop(RefreshType.logout);
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('Logged out'),
                        ),
                      );
                      await ref.read(aniCloudRepositoryProvider).logout();
                      ref.read(refreshProvider).refresh();
                    },
                    icon: const Icon(Icons.logout),
                    label: const Text('Logout'),
                  ),
                )
              ],
            )
          : const LoadingMessageWidget(),
    );
  }
}
