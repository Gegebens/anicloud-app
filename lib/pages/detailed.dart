import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:share_plus/share_plus.dart';

import '../extensions/extensions.dart';
import '../providers/detailed_page_provider.dart';
import '../providers/providers.dart';
import '../repositories/repositories.dart';
import '../util/util.dart';
import '../widgets/widgets.dart';

/// Page to view detailed information about a [BaseAnime] by transforming it into
/// an [DetailedAnime]
class Detailed extends ConsumerStatefulWidget {
  /// The basic data to fetch from
  final ParseAble parseAble;

  /// Used to identify the [Hero] widget animation
  final String url;

  /// The background image
  final String? imageUrl;

  /// A random number to differentiate between [Hero]es from multiple lists
  final int fortune;

  ///
  const Detailed({
    Key? key,
    required this.parseAble,
    required this.url,
    required this.fortune,
    this.imageUrl,
  }) : super(key: key);

  @override
  ConsumerState createState() => _DetailedState();
}

class _DetailedState extends ConsumerState<Detailed> {
  @override
  void initState() {
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      ref.read(currentParseAbles).state = widget.parseAble;
    });
    super.initState();
  }

  SliverAppBar _buildSliverAppBar(
      BuildContext context, DetailedAnime detailedAnime) {
    return SliverAppBar(
      pinned: true,
      backgroundColor: Colors.black.withOpacity(0.8),
      title: Text(detailedAnime.title),
      actions: <Widget>[
        FavoriteButton(
          parseAble: detailedAnime.parsed,
        ),
        IconButton(
          icon: const Icon(Icons.share),
          onPressed: () {
            Share.share(detailedAnime.parsed.toShare);
          },
        )
      ],
    );
  }

  Widget _buildFadedHeader(BuildContext context, DetailedAnime data) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
              colors: [
            Colors.black.withOpacity(0.0 * 0.0),
            Colors.black.withOpacity(0.1 * 0.1),
            Colors.black.withOpacity(0.2 * 0.2),
            Colors.black.withOpacity(0.3 * 0.3),
            Colors.black.withOpacity(0.4 * 0.4),
            Colors.black.withOpacity(0.5 * 0.5),
            Colors.black.withOpacity(0.6 * 0.6),
            Colors.black.withOpacity(0.7 * 0.7),
            Colors.black.withOpacity(0.8 * 0.8),
            Colors.black.withOpacity(0.9 * 0.9),
            Colors.black,
          ],
              stops: const [
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            1.0,
          ])),
      height: MediaQuery.of(context).size.height * 0.5,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: _buildFadedHeaderContent(context, data),
          ),
        ],
      ),
    );
  }

  Widget _buildFadedHeaderContent(
      BuildContext context, DetailedAnime detailedAnime) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '${detailedAnime.startYear}-${detailedAnime.endYear}',
                style: const TextStyle(fontSize: 18, color: Colors.white),
              ),
              Consumer(
                builder: (context, ref, child) {
                  final episodes = ref.watch(episodesProvider).data?.value;
                  return Text(
                    'Episoden: ${episodes?.length ?? '-'}',
                    style: const TextStyle(fontSize: 18, color: Colors.white),
                  );
                },
              ),
              Consumer(
                builder: (BuildContext context, WidgetRef ref, Widget? child) {
                  return DropdownButton<int>(
                    items: detailedAnime.seasons
                        .map(
                          (e) => DropdownMenuItem(
                            value: e,
                            child: e == -1
                                ? const Text('Filme / Specials')
                                : Text('Staffel $e'),
                          ),
                        )
                        .toList(),
                    onChanged: (value) {
                      if (value != null) {
                        ref
                            .read(episodeChooserProvider(widget.parseAble))
                            .state = 1;
                        ref
                            .read(seasonChooserProvider(widget.parseAble))
                            .state = value;
                      }
                    },
                    value: ref
                        .watch(seasonChooserProvider(widget.parseAble))
                        .state,
                  );
                },
              ),
            ],
          ),
          const Expanded(
            child: EpisodeChooser(),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: const CastFloatingActionButton(),
      bottomSheet: const CastBottomSheet(),
      body: RefreshIndicator(
        onRefresh: () async {
          DetailedCacheProvider().remove(Uri.parse(widget.url));
          final p = ref.read(currentParseAbles).state;
          ref.read(currentParseAbles).state = null;
          ref.read(currentParseAbles).state = p;
        },
        child: Stack(
          children: [
            if (widget.imageUrl != null)
              _buildImageHero(context, widget.imageUrl!)
            else
              ref.watch(detailedPageProvider)?.resolve(
                        data: (data) => _buildImageHero(context, data.image),
                      ) ??
                  const SizedBox(),
            ref.watch(detailedPageProvider)?.resolve(
                  data: (detailedAnime) {
                    return ScrollConfiguration(
                      behavior: NoScrollBehavior(),
                      child: CustomScrollView(
                        slivers: <Widget>[
                          _buildSliverAppBar(context, detailedAnime),
                          SliverList(
                            delegate: SliverChildListDelegate(
                              [
                                // Spacing Container
                                Container(
                                  height: MediaQuery.of(context).size.height *
                                      (1 / 5),
                                  //color: Colors.green,
                                  color: Colors.transparent,
                                ),
                                _buildFadedHeader(context, detailedAnime),
                                Container(
                                  color: Colors.black,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          bottom: 8,
                                          left: 8,
                                          right: 8,
                                        ),
                                        child: Text(detailedAnime.description),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Wrap(
                                          alignment: WrapAlignment.center,
                                          spacing: 5,
                                          children: detailedAnime.genres
                                              .where(
                                                (element) =>
                                                    // My favorite genre: Subtitles
                                                    element != 'GerSub' &&
                                                    element != 'Ger' &&
                                                    element != 'EngSub',
                                              )
                                              .map((e) => Chip(label: Text(e)))
                                              .toList(),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  },
                ) ??
                const SizedBox(),
          ],
        ),
      ),
    );
  }

  Hero _buildImageHero(BuildContext context, String imageUrl) {
    return Hero(
      tag: '${widget.url}${widget.fortune}',
      child: CachedNetworkImage(
        width: MediaQuery.of(context).size.width,
        imageUrl: imageUrl,
        fit: BoxFit.fitWidth,
        errorWidget: (context, _, __) => Container(),
        placeholder: (context, _) => Container(
          color: const Color(0xFF343434),
        ),
      ),
    );
  }
}
