library pages;

export 'all_animes.dart';
export 'auth/pages.dart';
export 'detailed/video_player_page.dart';
export 'detailed.dart';
export 'home.dart';
export 'network_share.dart';
export 'settings/logs.dart';
