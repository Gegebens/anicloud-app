import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import '../../extensions/extensions.dart';
import '../../providers/providers.dart';
import '../../widgets/widgets.dart';

class CalendarPage extends ConsumerWidget {
  const CalendarPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final calendarState = ref.watch(calendarProvider);
    return calendarState.resolve(
      data: (data) {
        return ListView.builder(
          itemCount: data.length,
          itemBuilder: (BuildContext context, int index) {
            final item = data.elementAt(index);
            return AnimeCardList(
              animes: item.animes,
              title:
                  '${item.day} - ${DateFormat('dd.MM.yyyy').format(item.date)} ${index == 0 ? 'Heute' : ''}',
            );
          },
        );
      },
    );
  }
}
