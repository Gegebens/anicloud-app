import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../extensions/extensions.dart';
import '../../providers/providers.dart';
import '../../repositories/repositories.dart';
import '../../widgets/big_message.dart';
import '../../widgets/widgets.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: [
        ValueListenableBuilder<Box<String>>(
          valueListenable: Hive.box<String>('favorites').listenable(),
          builder: (context, box, _) {
            return Consumer(
              builder: (context, ref, child) {
                final animes = ref.watch(
                  getAllFavorites(
                    box.values.map(
                      (e) => ParseAble(slug: e, season: 1),
                    ),
                  ),
                );
                if (animes.isEmpty) {
                  return const BigMessage(
                    icon: Icons.favorite,
                    message: 'Noch keine Favoriten hinzugefügt',
                  );
                }
                final result = animes.toList(growable: false);
                result.sort(
                    (u1, u2) => u1.toFetch.slug.compareTo(u2.toFetch.slug));
                return AnimeCardGrid(animes: result);
              },
            );
          },
        ),
        Consumer(
          builder: (BuildContext context, WidgetRef ref, Widget? child) {
            final watchList = ref.watch(watchListProvider);
            return watchList.resolve(
              data: (data) {
                if (data == null || data.isEmpty) {
                  return const BigMessage(
                    icon: Icons.favorite,
                    message: 'Noch keine Favoriten hinzugefügt',
                  );
                }
                final result = data.toList(growable: false);
                result.sort((u1, u2) => u1.slug.compareTo(u2.slug));
                return AnimeCardGrid(animes: result);
              },
            );
          },
        ),
      ],
    );
  }
}
