import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loggy/loggy.dart';

import '../../extensions/extensions.dart';
import '../../providers/providers.dart';
import '../../widgets/widgets.dart';

class HomePage extends ConsumerWidget with UiLoggy {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListView(
      children: [
        Consumer(
          builder: (BuildContext context, WidgetRef ref, Widget? child) {
            final state = ref.watch(subReleasesProvider);
            return state.resolve(
              data: (data) => AnimeCardList(
                animes: data,
                title: 'Airing',
              ),
            );
          },
        ),
        Consumer(
          builder: (BuildContext context, WidgetRef ref, Widget? child) {
            final state = ref.watch(dubReleasesProvider);
            return state.resolve(
              data: (data) => AnimeCardList(
                animes: data,
                title: 'Dubs',
              ),
            );
          },
        ),
        Consumer(
          builder: (BuildContext context, WidgetRef ref, Widget? child) {
            final state = ref.watch(homeProvider);
            return state.resolve(
              data: (data) {
                return Column(
                  children: data.carousels
                      .map(
                        (e) => AnimeCardList(
                          animes: e.animes,
                          title: e.name,
                        ),
                      )
                      .toList(),
                );
              },
            );
          },
        ),
      ],
    );
  }
}
