import 'package:flutter/material.dart';

import '../pages.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: const Icon(Icons.bug_report),
          title: const Text('Logs'),
          subtitle: const Text('Logs um Fehler zu beheben'),
          onTap: () => Navigator.of(context).pushNamed(Logs.routeName),
        )
      ],
    );
  }
}
