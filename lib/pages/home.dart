import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../providers/providers.dart';
import '../util/util.dart';
import '../widgets/widgets.dart';
import 'home/pages.dart';
import 'pages.dart';

/// First entrypoint after [App]
class Home extends ConsumerStatefulWidget {
  ///
  static const routeName = '/';

  ///
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends ConsumerState<Home> {
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    final acc = ref.watch(accountProvider).state;
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('AniCloud'),
          bottom: _currentPage == 2 && acc is LoggedInAccountState
              ? const TabBar(
                  tabs: [
                    Tab(
                      text: 'Lokal',
                    ),
                    Tab(
                      text: 'AniCloud',
                    )
                  ],
                )
              : null,
          actions: [
            if (acc is LoggedInAccountState)
              Builder(builder: (context) {
                return IconButton(
                  onPressed: () async {
                    final result = await Navigator.of(context)
                        .pushNamed(AccountPage.routeName);
                    if (result == RefreshType.logout) {
                      DefaultTabController.of(context)!.index = 0;
                    }
                  },
                  icon: CircleAvatar(
                    backgroundColor: Colors.orangeAccent,
                    child: acc.imageUrl.endsWith('svg')
                        ? Text(
                            acc.userName.substring(0, 2),
                            style: const TextStyle(color: Colors.white),
                          )
                        : Image.network(acc.imageUrl),
                  ),
                );
              })
            else
              IconButton(
                icon: const Icon(Icons.account_circle),
                onPressed: () {
                  Navigator.of(context).pushNamed(LoginPage.routeName);
                },
              ),
            IconButton(
              icon: const Icon(Icons.sort_by_alpha),
              onPressed: () {
                Navigator.of(context).pushNamed(AllAnimes.routeName);
              },
            ),
            IconButton(
              icon: const Icon(Icons.search),
              onPressed: () async {
                final found = await showSearch(
                  context: context,
                  delegate: AppSearchDelegate(),
                );
                if (found != null) {
                  Navigator.of(context).pushNamed(found);
                }
              },
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            ref.read(refreshProvider).refresh();
          },
          child: IndexedStack(
            index: _currentPage,
            children: const [
              HomePage(),
              CalendarPage(),
              FavoritePage(),
              SettingsPage(),
            ],
          ),
        ),
        floatingActionButton: const CastFloatingActionButton(),
        bottomSheet: const CastBottomSheet(),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentPage,
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(icon: Icon(Icons.event), label: 'Calendar'),
            BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Favorites'),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), label: 'Settings'),
          ],
          onTap: (index) => setState(() => _currentPage = index),
        ),
      ),
    );
  }
}
