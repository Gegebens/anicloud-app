import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:video_player/video_player.dart';

import '../repositories/repositories.dart';
import 'providers.dart';

enum SkipAction { backward, forward }

final hideControlsProvider = StateProvider<bool>((ref) => true);

final videoSourceProvider = StateProvider<StreamResultInfo?>((ref) {
  return null;
});

final videoPlayerControllerProvider =
    ChangeNotifierProvider.autoDispose<VideoPlayerController>((ref) {
  final video = ref.watch(videoSourceProvider).state;
  if (video != null) {
    final parseAble = ref.read(currentParseAbles).state!;
    final episode = ref.read(episodeChooserProvider(parseAble)).state;
    final season = ref.read(seasonChooserProvider(parseAble)).state;
    final key = '${parseAble.slug}_${season}_$episode';
    final initial = Duration(
        seconds: Hive.box<int>('timestamps').get(key, defaultValue: 0)!);
    final controller = VideoPlayerController.network(video.source.source!);

    Timer? markSeenTimer;
    if (ref.read(accountProvider).state is LoggedInAccountState) {
      markSeenTimer = Timer(const Duration(minutes: 2), () {
        final episodeData =
            ref.read(episodesProvider).data?.value?.elementAt(episode - 1);
        if (episodeData != null) {
          ref
              .read(aniCloudRepositoryProvider)
              .markEpisodeAsSeen(episodeData.episodeId);
        }
      });
    }

    controller.initialize().then((_) {
      controller.setVolume(1);
      controller.play().then((_) {
        controller.seekTo(initial);
      });
    });
    ref.onDispose(() {
      markSeenTimer?.cancel();
      if (controller.value.position.inSeconds > 0) {
        final progress = controller.value.position.inSeconds /
            controller.value.duration.inSeconds;
        ref.read(progressProvider(parseAble)).state = progress;
        Hive.box<double>('progress').put(key, progress);
        Hive.box<int>('timestamps').put(
          key,
          controller.value.position.inSeconds,
        );
      }
    });
    return controller;
  }
  return VideoPlayerController.network('');
});
