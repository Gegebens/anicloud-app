import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../repositories/repositories.dart';

final allAnimesProvider = FutureProvider.autoDispose((ref) async {
  final repo = ref.read(aniCloudRepositoryProvider);
  final response = await repo.getAllAnimes();
  ref.maintainState = true;
  return response;
});
