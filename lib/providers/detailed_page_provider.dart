import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';

import '../repositories/repositories.dart';
import 'providers.dart';

final currentParseAbles = StateProvider.autoDispose<ParseAble?>((_) => null);

final currentLanguageProvider = StateProvider<Language>((_) => Language.gerSub);

final seasonChooserProvider =
    StateProvider.family<int, ParseAble?>((ref, args) {
  final initial = Hive.box<int>('seasons')
      .get(args?.slug ?? '-', defaultValue: args?.season ?? 1);
  return initial!;
});

final episodeChooserProvider =
    StateProvider.family<int, ParseAble?>((ref, args) {
  final initial = Hive.box<int>('episodes')
      .get(args?.slug ?? '-', defaultValue: args?.episode ?? 1)!;
  return initial;
});

final progressProvider = StateProvider.family<double?, ParseAble>((ref, args) {
  final episode = ref.watch(episodeChooserProvider(args)).state;
  final season = ref.watch(seasonChooserProvider(args)).state;

  final key = '${args.slug}_${season}_$episode';
  return Hive.box<double>('progress').get(key);
});

final episodesProvider =
    FutureProvider.autoDispose<Iterable<Episode>?>((ref) async {
  final current = ref.read(currentParseAbles).state;
  final season = ref.watch(seasonChooserProvider(current));
  final repo = ref.read(aniCloudRepositoryProvider);

  if (current != null) {
    final response = await repo
        .getDetailedAnime(
          slug: current.slug,
          season: season.state,
        )
        .then((d) => d.episodes);

    final availableLang = response.first.languages.first;
    ref.read(currentLanguageProvider).state = availableLang;

    final lastRemoteWatched = response.lastWhere(
      (element) => element.watched,
      orElse: () => Episode('', '', [], -1, -1, true),
    );
    if (lastRemoteWatched.episode != -1) {
      ref.read(episodeChooserProvider(current)).state =
          lastRemoteWatched.episode;
    }
    return response;
  }
  return Future.value();
});

final detailedGetter =
    FutureProvider.family<DetailedAnime, ParseAble>((ref, parse) {
  final repo = ref.read(aniCloudRepositoryProvider);

  return repo.getDetailedAnime(
    slug: parse.slug,
    season: parse.season,
  );
});

final detailedPageProvider = Provider.autoDispose((ref) {
  final current = ref.watch(currentParseAbles);

  if (current.state != null) {
    return ref.watch(detailedGetter(current.state!));
  }
});

final canSkipProvider =
    Provider.family.autoDispose<bool, SkipAction>((ref, action) {
  final episodes = ref.watch(episodesProvider).data?.value;
  final episode = ref
      .watch(episodeChooserProvider(ref.watch(currentParseAbles).state))
      .state;
  if (action == SkipAction.forward) {
    return episode < (episodes?.length ?? 1);
  } else {
    return episode > 1;
  }
});
