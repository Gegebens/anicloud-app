import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../repositories/repositories.dart';
import '../util/util.dart';

final networkShareProvider = ChangeNotifierProvider((ref) {
  return NetworkShareProvider();
});

/// Manage the server and socket to provide the [NetworkSharePage] functionality
class NetworkShareProvider extends ChangeNotifier {
  StreamResultInfo? _info;
  HttpServer? _server;
  WebSocket? _socket;

  Future<String> get _placeholderHtml async {
    return rootBundle.loadString('assets/network_share/index.html');
  }

  bool get canStream => _info != null;

  /// The [_server] is ready to serve html
  bool get isReady => _server != null;

  /// The [_socket] has an open connection
  bool get isConnected =>
      _socket != null && _socket?.readyState != WebSocket.closed;

  RemoteVideoState _remoteVideoState = RemoteVideoState.remoteVideoOffline;

  /// The slider in [NetworkSharePage] is untouched, remote updates can be sent
  bool canUpdateDuration = true;
  double? _overridePosition;

  /// Set the current video position
  set overridePosition(double? position) {
    _overridePosition = position;
    notifyListeners();
  }

  /// Set the current video position
  double? get overridePosition => _overridePosition;

  /// The state of the video player
  RemoteVideoState get remoteVideoState => _remoteVideoState;

  /// Signal to the remote socket to refresh the site
  void refresh() {
    exec(() {
      _socket!.add('refresh:');
    });
  }

  /// Seek on the remote video player to the specified [position]
  void seek(double position) {
    exec(() {
      _socket!.add('seek:$position');
    });
  }

  /// Execute [seek] from the current time added to [value]
  void seekValue(double value) => seek(_remoteVideoState.currentTime + value);

  /// Sets the remote players status to ![remoteVideoState.playing]
  void playPause() {
    exec(() {
      _socket!.add('pause:${remoteVideoState.playing ? 1 : 0}');
    });
  }

  /// Execute [code] if [isConnected] is true
  void exec(VoidCallback code) {
    if (isConnected) {
      code();
    }
  }

  /// Close the [_server] and [_socket] connection and calls [cleanStream]
  void cleanup() {
    _server?.close(force: true);
    _socket?.close();
    _server = null;
    _socket = null;
    cleanStream();
  }

  void setStream(StreamResultInfo info) {
    _info = info;
    refresh();
    notifyListeners();
  }

  /// Reset the current streaming data
  void cleanStream() {
    _info = null;
    _remoteVideoState = RemoteVideoState.remoteVideoOffline;
    notifyListeners();
  }

  /// Start the [_server] on port 8080 to handle requests and accept [_socket]
  /// connections.
  Future<void> startCast() async {
    if (isReady) {
      return;
    }
    _server = await HttpServer.bind(InternetAddress.anyIPv4, 8080);
    notifyListeners();
    await for (final request in _server!) {
      if (request.uri.toString() == '/ws' &&
          WebSocketTransformer.isUpgradeRequest(request)) {
        exec(() {
          _socket!.close();
        });

        final socket = await WebSocketTransformer.upgrade(request);
        _socket = socket;
        notifyListeners();

        var lastCurrentTime = -1.0;
        _socket!.listen(
          (data) {
            if (canUpdateDuration && data is String) {
              if (!canStream) {
                refresh();
              } else {
                final state = data.split(';');
                final time = double.parse(state[0]);
                final duration = double.parse(state[1]);
                final paused = state[2] == 'true';

                final remoteState = RemoteVideoState(
                  time,
                  duration,
                  playing: !paused,
                  buffering: lastCurrentTime == time && !paused,
                );
                if (_remoteVideoState != remoteState) {
                  _remoteVideoState = remoteState;
                  notifyListeners();
                }

                if (paused) {
                  lastCurrentTime = -1.0;
                } else {
                  lastCurrentTime = time;
                }
              }
            }
          },
          onDone: () {
            _remoteVideoState = RemoteVideoState.remoteVideoOffline;
            notifyListeners();
          },
          onError: (e) {
            _remoteVideoState = RemoteVideoState.remoteVideoOffline;
            notifyListeners();
          },
          cancelOnError: true,
        );
      }
      /* else if (info?.host == Host.local &&
          request.uri.toString() == '/stream') {
        // Handle downloaded streams
        final videoFile = File(info.source.source.replaceFirst('file://', ''));
        if (canStream && await videoFile.exists()) {
          // Headers
          request.response.headers.contentType = ContentType('video', 'mp4');
          request.response.headers.set(HttpHeaders.acceptRangesHeader, 'bytes');
          final fileSize = await videoFile.length();

          // Handle ranges
          String rangeStr;
          var rangeLow = 0, rangeHigh = fileSize;
          if ((rangeStr = request.headers.value(HttpHeaders.rangeHeader)) !=
              null) {
            final range = HttpContentRange.parseFromRequest(rangeStr);

            if (range.end != null && range.end > rangeHigh) {
              request.response.statusCode =
                  HttpStatus.requestedRangeNotSatisfiable;
              request.response.close();
            }
            rangeLow = range.start;
            rangeHigh = range.end ?? fileSize;
            request.response.statusCode = HttpStatus.partialContent;

            request.response.headers.set(
                HttpHeaders.contentRangeHeader,
                HttpContentRange(
                        start: rangeLow, end: rangeHigh, size: fileSize)
                    .buildResponseHeader());

            request.response.contentLength = rangeHigh - rangeLow;
          } else {
            request.response.statusCode = HttpStatus.ok;
            request.response.contentLength = fileSize;
          }

          // stream video
          videoFile
              .openRead(rangeLow, rangeHigh)
              .pipe(request.response)
              .whenComplete(() {
            request.response.close();
          });
        }
      }*/
      else {
        request.response.headers.contentType =
            ContentType('text', 'html', charset: 'utf-8');
        if (canStream) {
          request.response.write('''
<head> <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet"/> <style type="text/css"> html, body, video{height: 100%; margin: 0; width: 100%; padding: 0; background: black;}video{margin: 0; position: absolute; top: 0;}</style> <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script> <title>Watch AniCloud</title></head><body><video autoplay class="video-js" controls data-setup='{"fluid": true}' id="my-video" preload="auto"> <source src="${_info?.host == Host.local ? '' /*TODO get IP*/ : _info?.source.source}" type="${(_info?.source.source?.endsWith('.mp4') ?? true) ? 'video/mp4' : 'application/x-mpegURL'}"/> <p class="vjs-no-js"> To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank" >supports HTML5 video</a > </p></video><script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
<script type="text/javascript">
    const player = videojs('my-video');
    const handle = (msg) => {
          console.log(msg);
          const query = msg.data.split(':');
          switch (query[0]) {
            case 'pause':
              if (query[1] === "1") {
                player.pause();
              } else {
                player.play();
              }
              break;
            case 'seek':
              player.currentTime(parseFloat(query[1]));
              break;
            case 'refresh':
              window.location.reload(true);
              break;
          }
        };
    let socket = new WebSocket('ws://' + window.location.host + '/ws');
    socket.onmessage = handle;
    player.ready(function () {
        setInterval(() => {
            if (socket.readyState !== WebSocket.OPEN) {
                socket = new WebSocket('ws://' + window.location.host + '/ws');
                socket.onmessage = handle;
            }
        }, 1000);
        setInterval(() => {
            if (socket.readyState === WebSocket.OPEN){
                socket.send(player.currentTime() + ';' + player.duration() + ';' + player.paused());
            }
        }, 500);
    });
</script>
</body>
''');
        } else {
          request.response.write(await _placeholderHtml);
        }
        request.response.close();
      }
    }
  }
}
