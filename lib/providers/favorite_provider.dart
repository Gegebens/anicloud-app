import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../repositories/repositories.dart';

final getAllFavorites =
    Provider.family<Iterable<UpgradeAble>, Iterable<ParseAble>>(
  (ref, args) {
    return args.map((e) {
      return UpgradeAble(e);
    });
  },
);
