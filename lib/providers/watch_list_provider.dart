import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:loggy/loggy.dart';

import '../repositories/repositories.dart';
import 'providers.dart';

final watchListRefreshProvider =
    ChangeNotifierProvider((ref) => RefreshProvider());

final watchListProvider = FutureProvider<Iterable<BaseAnime>?>((ref) {
  ref.watch(watchListRefreshProvider);
  ref.watch(refreshProvider);
  final account = ref.watch(accountProvider);
  final repo = ref.read(aniCloudRepositoryProvider);
  Loggy('watchListProvider').info(
      'Account state changed, refetching for ${account.state.runtimeType}');
  if (account.state is LoggedInAccountState) {
    return repo.getWatchList();
  }
  return Future.value();
});
