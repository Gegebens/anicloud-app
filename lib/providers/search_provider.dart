import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../repositories/repositories.dart';

final searchSuggestionsProvider = FutureProvider.autoDispose
    .family<Iterable<SearchResult>, String>((ref, query) {
  final repo = ref.read(aniCloudRepositoryProvider);
  return repo.search(query);
});

final searchResultsProvider = Provider.autoDispose
    .family<Iterable<UpgradeAble>, Iterable<SearchResult>>((ref, args) {
  return args.map((e) {
    final parsed = e.parsed;
    return UpgradeAble(parsed);
  });
});
