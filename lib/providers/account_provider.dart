import 'package:flutter_riverpod/flutter_riverpod.dart';

final accountProvider =
    StateProvider<AccountState>((_) => LoggedOutAccountState());

abstract class AccountState {
  String get readable;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccountState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class LoggedOutAccountState extends AccountState {
  @override
  String get readable => 'logged_out';
}

class LoggedInAccountState extends AccountState {
  final String userName;
  final String _imageUrl;

  String get imageUrl => 'https://anicloud.io/$_imageUrl';

  @override
  String get readable => 'logged_in';

  LoggedInAccountState(this.userName, this._imageUrl);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoggedInAccountState &&
          runtimeType == other.runtimeType &&
          userName == other.userName &&
          _imageUrl == other._imageUrl;

  @override
  int get hashCode => userName.hashCode ^ _imageUrl.hashCode;
}
