import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../repositories/repositories.dart';

final refreshProvider = ChangeNotifierProvider((_) => RefreshProvider());
final homeProvider = FutureProvider((ref) {
  ref.watch(refreshProvider);
  final repo = ref.read(aniCloudRepositoryProvider);

  return repo.getHomeResponse();
});
final _newReleasesProvider = FutureProvider((ref) {
  ref.watch(refreshProvider);
  final repo = ref.read(aniCloudRepositoryProvider);

  return repo.getLatestReleases();
});

final dubReleasesProvider = Provider((ref) {
  return ref.watch(_newReleasesProvider).whenData((value) => value
      .where((element) => element.releasedLanguage == Language.gerDub)
      .toSet());
});

final subReleasesProvider = Provider((ref) {
  return ref.watch(_newReleasesProvider).whenData((value) => value
      .where((element) => element.releasedLanguage == Language.gerSub)
      .toSet());
});

final calendarProvider = FutureProvider((ref) {
  ref.watch(refreshProvider);
  final repo = ref.read(aniCloudRepositoryProvider);
  return repo.getCalendar();
});

class RefreshProvider extends ChangeNotifier {
  void refresh() {
    notifyListeners();
  }
}
