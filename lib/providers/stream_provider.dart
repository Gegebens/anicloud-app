import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart';
import 'package:loggy/loggy.dart';

import '../exceptions/exceptions.dart';
import '../pages/pages.dart';
import '../repositories/repositories.dart';
import '../routes.dart';
import '../util/clients/clients.dart';
import '../widgets/captcha_dialog.dart';
import 'providers.dart';

final streamProvider = Provider(
  (ref) {
    final client = Client();
    return StreamProvider(
      {
        Host.voe: VoeClient(client),
        Host.vidoza: VidozaClient(client),
        Host.streamTape: StreamTapeClient(client),
      },
      ref.read,
    );
  },
);

class StreamProvider with UiLoggy {
  final Map<Host, StreamClient> _clients;
  final Reader _reader;
  final Map<
      // slug
      String,
      Map<
          // season
          int,
          Map<
              // episode
              int,
              Map<
                  // language
                  Language,
                  StreamCacheEntry>>>> _cache = {};

  AniCloudRepository get _repository => _reader(aniCloudRepositoryProvider);

  StreamProvider(this._clients, this._reader);

  Future<StreamResultInfo?> _getPlayer(
    String slug,
    int season,
    int episode,
    Language language,
  ) async {
    final hoster = (await _repository.getHostData(
      slug: slug,
      season: season,
      episode: episode,
    ))
        .where((element) => element.language == language)
        .toList();
    hoster.sort((h1, h2) => h1.host.index.compareTo(h2.host.index));

    final isLoggedIn = _reader(accountProvider).state is LoggedInAccountState;

    for (final hostData in hoster) {
      String? url;
      String? cookies;

      if (isLoggedIn) {
        try {
          url = await _repository.resolveRedirectLink(hostData);
        } on CaptchaDistrustException catch (e) {
          cookies = e.cookieString;
        }
      }

      url ??= await showDialog<String>(
        context: kNavigatorKey.currentContext!,
        builder: (context) {
          return CaptchaDialog(
            redirectLink: hostData.redirectLink,
            cookies: cookies,
          );
        },
      );
      if (url != null) {
        final origin = HostResultInfo(url, hostData.host);
        final player = await _get(url, slug, season, episode, hostData.host);
        if (player != null) {
          final toCache = StreamCacheEntry(player, origin);
          _putToCache(slug, season, episode, language, toCache);
          return player;
        }
      }
    }
    return null;
  }

  void _putToCache(String slug, int season, int episode, Language language,
      StreamCacheEntry entry) {
    if (!_cache.containsKey(slug)) {
      _cache.putIfAbsent(
        slug,
        () => {
          season: {
            episode: {language: entry}
          },
        },
      );
    } else {
      if (_cache[slug]!.containsKey(season)) {
        _cache[slug]![season]!.putIfAbsent(episode, () => {language: entry});
      } else {
        _cache[slug]!.putIfAbsent(
            season,
            () => {
                  episode: {language: entry}
                });
      }
    }
  }

  /// acquire [StreamResultInfo]
  Future<StreamResultInfo?> resolveStreamSource(
      String slug, int season, int episode, Language language) async {
    Hive.box<int>('episodes').put(slug, episode);
    Hive.box<int>('seasons').put(slug, season);

    final source =
        await _getStreamHosterFromCache(slug, season, episode, language);
    // get new response if cache misses
    return source ?? await _getPlayer(slug, season, episode, language);
  }

  ParseAble? get _parseAble => _reader(currentParseAbles).state;

  Future<StreamResultInfo?> _resolveForCurrentValues(int chosenEpisode) async {
    final chosenSeason = _reader(seasonChooserProvider(_parseAble)).state;
    var language = _reader(currentLanguageProvider).state;

    final detailedAnime = _reader(detailedPageProvider)!.data!.value;
    final episodes = _reader(episodesProvider).data!.value!;

    final availableLanguages = episodes.elementAt(chosenEpisode - 1).languages;
    if (!availableLanguages.contains(language)) {
      language = availableLanguages.first;
    }

    return resolveStreamSource(
      detailedAnime.slug,
      chosenSeason,
      chosenEpisode,
      language,
    );
  }

  Future<void> openPlayer() async {
    if (await replacePlayer()) {
      if (_reader(networkShareProvider).isReady) {
        Navigator.of(kNavigatorKey.currentContext!)
            .pushNamed(NetworkShare.routeName);
      } else {
        Navigator.of(kNavigatorKey.currentContext!)
            .pushNamed(VideoPlayerPage.routeName);
      }
    }
  }

  Future<bool> replacePlayer() async {
    final episode = _reader(episodeChooserProvider(_parseAble)).state;
    final streamData = await _resolveForCurrentValues(episode);
    if (streamData != null) {
      if (_reader(networkShareProvider).isReady) {
        _reader(networkShareProvider).setStream(streamData);
      } else {
        _reader(videoSourceProvider).state = streamData;
      }
      return true;
    }
    return false;
  }

  /// Loads the stream url from cache, if it is available
  Future<StreamResultInfo?> _getStreamHosterFromCache(
    String slug,
    int season,
    int episode,
    Language language,
  ) async {
    final cached = _cache[slug]?[season]?[episode]?[language];
    if (cached == null) {
      return null;
    }
    if (await cached.isValid) {
      return cached.info;
    }
    loggy.warning('Refetching cached episode entry');
    final reFetched = await _get(
        cached.originalData.resolved, slug, season, episode, cached.info.host);
    if (reFetched != null) {
      _cache[slug]![season]![episode]![language] =
          StreamCacheEntry(reFetched, cached.originalData);
    }
    return reFetched;
  }

  Future<StreamResultInfo?> _get(
    String url,
    String slug,
    int season,
    int episode,
    Host host,
  ) async {
    final id = StreamId.tryParseStreamId(url, host);
    if (id == null) return null;
    try {
      final page = await _clients[host]!.getVideo(
        id,
        slug,
        season,
        episode,
        headers: {
          HttpHeaders.refererHeader: 'https://anicloud.io/',
        },
      );
      return StreamResultInfo(source: page, host: host);
    } on VideoUnavailableException catch (e) {
      loggy.warning('Stream for host ${host.readable} is gone', e);
      return null;
    }
  }
}
