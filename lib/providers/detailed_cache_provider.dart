import '../repositories/repositories.dart';

class DetailedCacheProvider {
  factory DetailedCacheProvider() => _instance ??= DetailedCacheProvider._();

  final Map<Uri, DetailedAnime> _animes = {};

  DetailedCacheProvider._();

  static DetailedCacheProvider? _instance;

  DetailedAnime? get(Uri uri) => _animes[uri];

  void set(Uri uri, DetailedAnime anime) {
    _animes[uri] = anime;
  }

  void remove(Uri uri) => _animes.remove(uri);
}
