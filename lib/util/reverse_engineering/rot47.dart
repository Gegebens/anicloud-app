/// Rotates every character in a string by 47 bytes
///
/// This is used by [ViVoSXClient] as an obfuscation
const _Rot47 rot47 = _Rot47();

class _Rot47 {
  const _Rot47();

  String convert(String input) {
    return String.fromCharCodes(input.runes.map((char) {
      if (char != 32) {
        char += 47;
        if (char > 126) {
          char -= 94;
        }
      }
      return char;
    }));
  }
}
