/// The standard depack algorithm used for [GoUnlimitedClient]
const dePack = _DePack();

class _DePack {
  const _DePack();

  String convert(String p, int a, int c, List<String> k) {
    if (c == 0) return p;
    final re = RegExp('\\b${c.toRadixString(a)}\\b');
    try {
      final data = k[c];
      final res = p.replaceAll(re, data);
      return convert(res, a, c - 1, k);
    } on Exception {
      return convert(p, a, c - 1, k);
    }
  }
}
