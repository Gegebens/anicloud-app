import 'package:flutter/material.dart';

final kThemeData = ThemeData(
  pageTransitionsTheme: const PageTransitionsTheme(
    builders: {
      TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
    },
  ),
  scaffoldBackgroundColor: const Color(0xFF121c22),
  colorScheme: const ColorScheme.dark(secondary: Color(0xFF495bb5)),
  appBarTheme: const AppBarTheme(color: Color(0xFF637cf9)),
  brightness: Brightness.dark,
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: ButtonStyle(
      side: MaterialStateProperty.all(
        const BorderSide(
          color: Colors.white,
          width: 2,
        ),
      ),
      foregroundColor: MaterialStateProperty.all(Colors.white),
    ),
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: Color(0xFF1b2431),
  ),
  sliderTheme: SliderThemeData(
    activeTrackColor: const Color(0xFFe3227f),
    inactiveTrackColor: const Color(0xFFf071ae).withOpacity(0.3),
    thumbColor: const Color(0xFFd61a75),
  ),
  inputDecorationTheme:
      const InputDecorationTheme(border: OutlineInputBorder()),
);
