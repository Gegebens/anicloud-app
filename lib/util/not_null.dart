bool notNull<E>(List<E?> data) {
  for (final item in data) {
    if (item == null) {
      return false;
    }
  }
  return true;
}
