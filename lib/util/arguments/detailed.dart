class DetailedArguments {
  final int fortune;
  final String imageUrl;

  DetailedArguments(this.fortune, this.imageUrl);
}
