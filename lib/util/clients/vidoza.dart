import 'package:html/dom.dart';
import 'package:http/http.dart' as http;

import 'clients.dart';

class _ResponsePage extends ResponsePage<Document> {
  final String _root;

  _ResponsePage(this._root);

  @override
  String? get source {
    final source = transform.querySelector('source');
    return source?.attributes['src'];
  }

  @override
  Document get transform => Document.html(_root);
}

/// Client to get the videostreams from vidoza.to
class VidozaClient extends StreamClient {
  ///
  VidozaClient(http.Client client) : super(client);

  @override
  Host get host => Host.vidoza;

  @override
  ResponsePage transform(http.Response response) =>
      _ResponsePage(response.body);
}
