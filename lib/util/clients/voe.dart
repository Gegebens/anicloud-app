import 'package:http/http.dart' as http;

import 'clients.dart';

class _ResponsePage extends ResponsePage<String> {
  final String _root;

  _ResponsePage(this._root);

  @override
  String? get source {
    final playerData = RegExp(r'const\ssources\s=\s{\n\t*"hls":\s"(.*.m3u8)",')
        .firstMatch(transform);

    if (playerData == null) {
      return null;
    }
    return playerData.group(1);
  }

  @override
  String get transform => _root;
}

/// Client to get the videostreams from voe.sx
class VoeClient extends StreamClient {
  ///
  VoeClient(http.Client client) : super(client);

  @override
  Host get host => Host.voe;

  @override
  ResponsePage transform(http.Response response) =>
      _ResponsePage(response.body);
}
