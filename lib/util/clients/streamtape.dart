import 'package:html/dom.dart';
import 'package:http/http.dart' as http;
import 'package:loggy/loggy.dart';

import 'clients.dart';

class _ResponsePage extends ResponsePage<Document> with UiLoggy {
  final String _root;

  _ResponsePage(this._root);

  @override
  String? get source {
    late String sourceElement;
    try {
      sourceElement = transform
          .querySelectorAll('script')
          .firstWhere((element) {
            final replaced =
                element.text.replaceAll(RegExp(r'''["'\s+\(\)]'''), '');
            return replaced.contains('streamtape.com') ||
                replaced.contains('get_video') ||
                replaced.contains('getElementByIdvideoolink');
          })
          .text
          // How much effort did you put into anti-scraping measures? Maximum effort xD
          // document.getElementById('vid'+'eolink').innerHTML = "//strea" + 'mtape.com/get_video?id=_&expires=_&ip=_&token=_';
          .replaceAll(RegExp(r'''["'\s+\(\)]'''), '');
    } catch (e, s) {
      loggy.error('StreamTape source failed', e, s);
      sourceElement = '';
    }
    if (sourceElement.isEmpty) return null;
    final sourceRegex = RegExp(r"\/\/(streamtape\.com\/.*).substring\d*;");
    final matched = sourceRegex.firstMatch(sourceElement)?.group(1);
    if (matched == null) return null;
    final parsed = Uri.tryParse('https://$matched')?.queryParameters;
    if (parsed == null) return null;
    if (!parsed.containsKey('id') ||
        !parsed.containsKey('expires') ||
        !parsed.containsKey('ip') ||
        !parsed.containsKey('token')) {
      return null;
    }
    return 'https://streamtape.com/get_video?id=${parsed['id']}&expires=${parsed['expires']}&ip=${parsed['ip']}&token=${parsed['token']}&stream=1';
  }

  @override
  Document get transform => Document.html(_root);
}

/// Client to get the videostreams from streamtape.com
class StreamTapeClient extends StreamClient {
  ///
  StreamTapeClient(http.Client client) : super(client);

  @override
  Host get host => Host.streamTape;

  @override
  ResponsePage transform(http.Response response) =>
      _ResponsePage(response.body);
}
