import 'clients.dart';

/// Helper to load local episodes
class LocalResponsePage extends ResponsePage<String> {
  final String _path;

  ///
  LocalResponsePage(this._path);

  @override
  String get source => _path;

  @override
  String? get transform => null;
}
