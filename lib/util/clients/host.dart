import 'clients.dart';

enum Host { voe, vidoza, streamTape, ninjaStream, local, unknown }

extension HostExt on Host {
  /// Returns the url for the [Host] to request with the [StreamId]
  Uri requestUrl(StreamId id) {
    return Uri.parse(() {
      switch (this) {
        case Host.streamTape:
          return 'https://streamtape.com/e/$id/';
        case Host.vidoza:
          return 'https://vidoza.net/$id.html';
        case Host.voe:
          return 'https://voe.sx/e/$id';
        default:
          throw UnimplementedError();
      }
    }());
  }

  static Host parse(String name) {
    switch (name) {
      case 'VOE':
        return Host.voe;
      case 'NinjaStream':
        return Host.ninjaStream;
      case 'Streamtape':
        return Host.streamTape;
      case 'Vidoza':
        return Host.vidoza;
      default:
        return Host.unknown;
    }
  }

  /// Gives back a name with the String
  String get readable {
    switch (this) {
      case Host.vidoza:
        return 'vidoza.net';
      case Host.streamTape:
        return 'streamtape.net';
      case Host.voe:
        return 'voe.sx';
      case Host.local:
        return 'Offline Stream';
      default:
        return 'Unknown';
    }
  }
}
