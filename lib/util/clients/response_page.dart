import 'package:loggy/loggy.dart';

import '../../exceptions/exceptions.dart';
import 'clients.dart';

/// Interface for the [StreamClient] to parse data
abstract class ResponsePage<E> with UiLoggy {
  /// The final source url
  String? get source;

  /// Check if the source is valid
  bool get isAvailable => source != null;

  /// Check if the source is invalid
  bool get isNotAvailable => !isAvailable;

  /// Checks if the source is available, throws [VideoUnavailableException]
  void checkAvailable(
    Host host,
    String slug,
    int season,
    int episode,
  ) {
    if (isNotAvailable) {
      throw VideoUnavailableException(slug, episode, season);
    } else {
      loggy.info('Got stream from host ${host.readable}');
    }
  }

  /// Transform the source by parsing it i.e. into an html document
  E? get transform;
}
