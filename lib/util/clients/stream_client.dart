import 'package:http/http.dart';

import 'clients.dart';

/// An interface for every streamhoster implementation to get the video
abstract class StreamClient<E> {
  final Client _client;

  /// The request client
  StreamClient(this._client);

  /// The host to request
  Host get host;

  /// The exact implementor of [ResponsePage]
  ResponsePage<E> transform(Response response);

  /// Retrieve a video source for the [id] with the [host]
  Future<ResponsePage<E>> getVideo(
    StreamId id,
    String slug,
    int season,
    int episode, {
    Map<String, String> headers = const {},
  }) async {
    final response = await _client.get(host.requestUrl(id), headers: headers);
    final transformed = transform(response);
    transformed.checkAvailable(host, slug, season, episode);
    return transformed;
  }
}
