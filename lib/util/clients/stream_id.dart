import 'host.dart';

/// Used to parse string into valid ids
class StreamId {
  final String _id;
  static final Map<Host, RegExp> _matchers = {
    Host.vidoza: RegExp(r'https:\/\/vidoza\.net\/embed-([a-z0-9]{12})\.html'),
    Host.streamTape:
        RegExp(r'https:\/\/streamtape\.com\/e\/([a-zA-Z0-9]{10,20})'),
    Host.voe: RegExp(r'https:\/\/voe.sx\/e\/([a-zA-Z0-9]{10,20})')
  };

  StreamId._(this._id);

  /// Checks if the [id] contains valid chars
  static bool _validateStandardId(String id, {int min = 10, int max = 20}) {
    if (id.trim().isEmpty || id.length < min || id.length > max) {
      return false;
    }
    return !RegExp('[^0-9a-zA-Z]').hasMatch(id);
  }

  /// Parse a video Id according to the specified streamhoster
  static StreamId? tryParseStreamId(String idOrUrl, Host host) {
    if (idOrUrl.trim().isEmpty) return null;
    if (_validateStandardId(idOrUrl)) {
      return StreamId._(idOrUrl);
    }
    final regex = _matchers[host];
    final id = regex?.firstMatch(idOrUrl)?.group(1);
    if (id == null) {
      return null;
    }
    if (_validateStandardId(id)) {
      return StreamId._(id);
    }
    return null;
  }

  @override
  String toString() => _id;
}
