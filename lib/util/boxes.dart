import 'package:hive/hive.dart';

class _Opener<E> {
  final String _name;

  const _Opener(this._name);

  Future open() => Hive.openBox<E>(_name);
}

const kBoxes = [
  _Opener<String>('favorites'),
  _Opener<int>('timestamps'),
  _Opener<double>('progress'),
  _Opener<int>('seasons'),
  _Opener<int>('episodes'),
  _Opener<bool>('settings'),
];
