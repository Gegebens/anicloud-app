import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../extensions/extensions.dart';
import '../providers/providers.dart';
import '../widgets/big_message.dart';
import '../widgets/widgets.dart';

class AppSearchDelegate extends SearchDelegate<String?> {
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
      IconButton(
        icon: const Icon(Icons.search),
        onPressed: () {
          showResults(context);
        },
      )
    ];
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    return Theme.of(context).copyWith(
        inputDecorationTheme:
            const InputDecorationTheme(border: UnderlineInputBorder()));
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return query.isEmpty
        ? const SizedBox()
        : Consumer(
            builder: (BuildContext context, WidgetRef ref, Widget? child) {
              final result = ref.watch(searchSuggestionsProvider(query));
              return result.resolve(
                data: (data) {
                  final result = ref.watch(searchResultsProvider(data));
                  return data.isEmpty
                      ? const BigMessage(
                          icon: Icons.sentiment_dissatisfied,
                          message: 'Nichts gefunden',
                        )
                      : AnimeCardGrid(
                          animes: result,
                          callback: (parseAble) {
                            close(context, parseAble);
                          },
                        );
                },
              );
            },
          );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return query.isEmpty
        ? const SizedBox()
        : Consumer(
            builder: (BuildContext context, WidgetRef ref, Widget? child) {
              final result = ref.watch(searchSuggestionsProvider(query));
              return result.resolve(
                data: (data) {
                  return data.isEmpty
                      ? const BigMessage(
                          icon: Icons.sentiment_dissatisfied,
                          message: 'Nichts gefunden',
                        )
                      : ListView.builder(
                          itemCount: data.length,
                          itemBuilder: (BuildContext context, int index) {
                            final item = data.elementAt(index);
                            return ListTile(
                              title: Text(item.name),
                              subtitle: Text(item.description),
                              trailing: FavoriteButton(
                                parseAble: item.parsed,
                              ),
                              leading: SizedBox(
                                height: double.infinity,
                                child: AspectRatio(
                                  aspectRatio: 9 / 16,
                                  child: CachedNetworkImage(
                                    imageUrl: item.coverUrl,
                                  ),
                                ),
                              ),
                              onTap: () {
                                close(context, item.parsed.toShare);
                              },
                            );
                          },
                        );
                },
              );
            },
          );
  }
}
