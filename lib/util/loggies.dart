import 'package:loggy/loggy.dart';

mixin ParserLoggy implements LoggyType {
  @override
  Loggy<NetworkLoggy> get loggy =>
      Loggy<NetworkLoggy>('Parser Loggy - ${runtimeType.toString()}');
}
