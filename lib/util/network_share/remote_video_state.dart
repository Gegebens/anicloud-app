import 'package:flutter/material.dart';

/// Container for information about video state on remote device
@immutable
class RemoteVideoState {
  /// Current playback timestamp
  final double currentTime;

  /// Video length
  final double _duration;

  /// currently playing
  final bool playing;

  /// currently buffering
  final bool buffering;

  /// don't display NaNs
  double get duration => _duration.isNaN ? 1 : _duration;

  /// Constructor
  const RemoteVideoState(
    this.currentTime,
    this._duration, {
    required this.playing,
    required this.buffering,
  }) : super();

  /// Determine if given video offline
  bool get isOffline => this == remoteVideoOffline;

  /// Determine if given video online
  bool get isOnline => !isOffline;

  /// Remote Video Offline
  static const RemoteVideoState remoteVideoOffline =
      RemoteVideoState(-1, -1, playing: false, buffering: false);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RemoteVideoState &&
          runtimeType == other.runtimeType &&
          currentTime == other.currentTime &&
          _duration == other._duration &&
          playing == other.playing;

  @override
  int get hashCode =>
      currentTime.hashCode ^ _duration.hashCode ^ playing.hashCode;
}
