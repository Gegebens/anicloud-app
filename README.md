# AniCloud App

Einfach auf dem Handy https://anicloud.io benutzen, ohne Werbung!

### [Download](https://github.com/Gegebenes/anicloud_app/releases/latest)

## Features

- [x] HomePage anzeigen
- [x] Anime Detail Seite
- [x] Animes Suchen
- [x] Episoden gucken
    - [x] Vorspulen, Zurück etc.
- [x] Account verbinden
    - [x] Einloggen, Ausloggen
    - [x] Watchlisten synchronisieren
- [ ] Streamhoster auswählen

### Unterstüzte Streamhoster

- [x] VOE
- [ ] NinjaStream
- [x] StreamTape
- [x] Vidoza

## For developers

Feel free to open pull requests to add in your desired features
